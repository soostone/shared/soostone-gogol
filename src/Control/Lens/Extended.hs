module Control.Lens.Extended
  ( module Control.Lens,
    previewTH,
    autoPrism,
    autoPrismWith,
    base64Encoded,
  )
where

-------------------------------------------------------------------------------

import Control.Error
import Control.Lens hiding (universe)
import Data.ByteString (ByteString)
import qualified Data.ByteString.Base64 as B64
import qualified Data.Map.Strict as M
import qualified Data.Universe.Class as Universe
import Language.Haskell.TH (Exp, Q)
import Language.Haskell.TH.Syntax (Lift)
import qualified Language.Haskell.TH.Syntax as THS
import Prelude

-------------------------------------------------------------------------------

-- | 'preview' a 'Prism' at compile-time with some known input.
--
-- This is useful in situations where we have data types whose only exposed
-- smart constructors are 'Prism's, but from which we would like to create
-- instances with known-good input literals.
--
-- For example:
--
-- @
--     mkNonEmptyStrTH inputStr =
--       previewTH
--         "Expected a non-empty input string"
--         nonEmptyStrPrism
--         inputStr
-- @
--
-- If @mkNonEmptyStrTH@ were called with @""@ as input, it would fail at
-- compile-time.
previewTH ::
  Lift supertype =>
  String ->
  Prism' subtype supertype ->
  subtype ->
  Q Exp
previewTH er pris input = case preview pris input of
  Nothing -> fail er
  Just result -> THS.lift result

-------------------------------------------------------------------------------

-- | This is appropriate to use in cases where `a` is a finite and
-- relatively small universe, e.g. a sum type. You can derive Universe
-- via Generic. Given a total mapping in one direction, this provides
-- a prism that reverses the mapping. AFAIK the map this uses
-- internally is only generated once per mapping function. It is
-- strongly recommended you use a case of pattern matching in your
-- mapping function so that it stays updated when new cases are added.
autoPrism ::
  ( Universe.Universe a,
    Ord b
  ) =>
  (a -> b) ->
  Prism' b a
autoPrism = autoPrismWith mempty

-- | 'autoPrism' with additional mappings bath the other way for
-- backwards compatibility.
autoPrismWith ::
  ( Universe.Universe a,
    Ord b
  ) =>
  -- | Additional mappings
  M.Map b a ->
  (a -> b) ->
  Prism' b a
autoPrismWith extraMappings aToB = prism' aToB (flip M.lookup bToA)
  where
    -- mappend is left-biased
    !bToA =
      M.fromList [(aToB a, a) | a <- Universe.universe]
        <> extraMappings

base64Encoded :: Prism' ByteString ByteString
base64Encoded = prism' B64.encode (hush . B64.decode)
