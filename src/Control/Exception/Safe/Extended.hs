module Control.Exception.Safe.Extended
  ( module Control.Exception.Safe,
    catchIf,
  )
where

-------------------------------------------------------------------------------

import Control.Exception.Safe
import Prelude

-------------------------------------------------------------------------------

catchIf ::
  ( MonadCatch m,
    Exception e
  ) =>
  (e -> Bool) ->
  m a ->
  (e -> m a) ->
  m a
catchIf match = catchJust (\e -> if match e then Just e else Nothing)
