{-# LANGUAGE LambdaCase #-}

-- | The default gogol-bigquery schema is extremely loosely
-- typed. This is a subset of the schema that should cover all of our
-- use cases while providing richer types.
module Soostone.Google.BigQuery.Schema
  ( -- * Types
    Schema (..),
    schema_order,
    schema_fields,
    FieldSchema (..),
    fieldSchema_mode,
    fieldSchema_type,
    FieldName,
    fieldNameText,
    mkFieldNameTH,
    FieldMode (..),
    fieldModeText,
    FieldType (..),
    FieldTypeScalar (..),
    fieldTypeScalarTextBQ,
    fieldTypeScalarTextCustom,
    toTableSchema,
    fromTableSchemaLenient,
    fromTableSchema,
    downgradeSchema,

    -- ** Values
    Value (..),
    valueFieldType,
    parseValueHint,

    -- * Time
    BQTimestamp (..),
    bigQueryDateFormatString,
    bigQueryTimeFormatString,
    bigQueryTimeFormatStrings,
    bigQueryDateTimeFormatString,
    bigQueryDateTimeFormatStrings,
    bigQueryTimestampFormatString,
    bigQueryTimestampFormatStrings,
    parseBqTimestamp,

    -- ** Value Prisms
    _Value_Int64,
    _Value_Numeric,
    _Value_Float64,
    _Value_Bool,
    _Value_String,
    _Value_Bytes,
    _Value_Date,
    _Value_DateTime,
    _Value_Timestamp,
    _Value_Time,
    _Value_JSON,
    _Value_Null,

    -- * Constructing schemas
    mkSchema,
    required,
    nullable,

    -- ** Construct fields by type
    intField,
    numericField,
    floatField,
    boolField,
    stringField,
    bytesField,
    dateField,
    dateTimeField,
    timeField,
    timestampField,
    jsonField,
    arrayOf,

    -- * Exported for testing
    mkFieldSchema,
  )
where

-------------------------------------------------------------------------------
import Control.Lens.Extended
import qualified Data.Aeson.Extended as A
import qualified Data.Aeson.Safe as AS
import qualified Data.CaseInsensitive as CI
import qualified Data.HashMap.Strict as HM
import Data.Hashable (Hashable)
import qualified Data.List.NonEmpty as NE
import qualified Data.Map.Strict as M
import qualified Data.String.Conv as SC
import qualified Data.Text as T
import qualified Data.Time.Format as Time
import qualified Data.Time.LocalTime as Time
import qualified Data.Validation as V
import qualified Gogol.BigQuery as BQ
import Instances.TH.Lift ()
import Language.Haskell.TH (Exp, Q)
import qualified Language.Haskell.TH.Syntax as THS
import Soostone.Google.Prelude

-------------------------------------------------------------------------------

newtype FieldName = FieldName
  { _fieldName :: Text
  }
  deriving stock (Show, Eq, Ord, THS.Lift)
  deriving newtype (Hashable, A.ToJSON, A.FromJSON, A.ToJSONKey, A.FromJSONKey)

instance AS.SafeJSON FieldName where
  version = 0
  kind = AS.base

-- | [a-zA-Z_][a-zA-Z0-9_]{0,127}
fieldNameText :: Prism' Text FieldName
fieldNameText = prism' _fieldName $ \txt -> do
  (firstChar, remaining) <- T.uncons txt
  guard ((isAlphaASCII <||> (== '_')) firstChar)
  guard (T.all (isAlphaNumASCII <||> (== '_')) remaining)
  guard (T.length txt <= 128)
  pure (FieldName txt)

-- | Make a field name that you statically know is valid:
-- @$(mkFieldNameTH "a_valid_field_name")@
mkFieldNameTH :: Text -> Q Exp
mkFieldNameTH text =
  previewTH
    ("Expected a valid FieldName ([a-zA-Z_][a-zA-Z0-9_]{0,127}), received: " <> T.unpack text)
    fieldNameText
    text

-------------------------------------------------------------------------------
data FieldSchema = FieldSchema
  { _fieldSchema_mode :: !FieldMode,
    _fieldSchema_type :: !FieldType
  }
  deriving stock (Show, Eq)

instance A.ToJSON FieldSchema where
  toJSON s =
    A.object
      [ "mode" A..= _fieldSchema_mode s,
        "type" A..= _fieldSchema_type s
      ]

instance A.FromJSON FieldSchema where
  parseJSON = A.withObject "FieldSchema" $ \o ->
    FieldSchema
      <$> o A..: "mode"
      <*> o A..: "type"

instance AS.SafeJSON FieldSchema where
  version = 0
  kind = AS.base
  safeFrom = AS.containWithObject "FieldSchema" $ \o -> do
    mode <- o AS..:$ "mode"
    ty <- o AS..:$ "type"
    pure $
      FieldSchema
        { _fieldSchema_mode = mode,
          _fieldSchema_type = ty
        }
  safeTo fs =
    AS.contain $
      AS.object
        [ "mode" AS..=$ _fieldSchema_mode fs,
          "type" AS..=$ _fieldSchema_type fs
        ]

-------------------------------------------------------------------------------
-- omitting repeated for now
data FieldMode
  = FieldMode_Nullable
  | FieldMode_Required
  deriving stock (Show, Eq, Enum, Bounded, Ord)

instance Universe FieldMode

instance A.ToJSON FieldMode where
  toJSON = A.toJSON . CI.original . review fieldModeText

instance A.FromJSON FieldMode where
  parseJSON v = do
    t <- A.parseJSON v
    maybe
      (fail ("Could not parse FieldMode from " <> T.unpack t))
      pure
      (preview fieldModeText (CI.mk t))

fieldModeText :: Prism' (CI.CI Text) FieldMode
fieldModeText = autoPrism $ \case
  FieldMode_Nullable -> "NULLABLE"
  FieldMode_Required -> "REQUIRED"

instance AS.SafeJSON FieldMode where
  version = 0
  kind = AS.base

-------------------------------------------------------------------------------
data FieldType
  = FieldType_Scalar !FieldTypeScalar
  | -- | App-specific: we encode this as A because CSV import/export doesn't support arrays.
    FieldType_Array !FieldTypeScalar
  deriving stock (Show, Eq, Generic)

instance A.ToJSON FieldType where
  toJSON (FieldType_Scalar s) = A.object ["scalar" A..= s]
  toJSON (FieldType_Array s) = A.object ["array" A..= s]

instance A.FromJSON FieldType where
  parseJSON = A.withObject "FieldType" (\o -> parseScalar o <|> parseArray o)
    where
      parseScalar o = FieldType_Scalar <$> o A..: "scalar"
      parseArray o = FieldType_Array <$> o A..: "array"

instance AS.SafeJSON FieldType where
  version = 0
  kind = AS.base

-------------------------------------------------------------------------------

-- | <https://cloud.google.com/bigquery/docs/reference/standard-sql/data-types>
data FieldTypeScalar
  = FieldTypeScalar_Int64
  | FieldTypeScalar_Numeric
  | FieldTypeScalar_Float64
  | FieldTypeScalar_Bool
  | FieldTypeScalar_String
  | FieldTypeScalar_Bytes
  | FieldTypeScalar_Date
  | FieldTypeScalar_DateTime
  | FieldTypeScalar_Time
  | FieldTypeScalar_Timestamp
  | -- | We encode this as FString
    FieldTypeScalar_JSON
  deriving stock (Show, Eq, Enum, Bounded, Ord, Generic)

instance Universe FieldTypeScalar

instance A.ToJSON FieldTypeScalar where
  toJSON = A.toJSON . CI.original . review fieldTypeScalarTextBQ

instance A.FromJSON FieldTypeScalar where
  parseJSON v = do
    t <- A.parseJSON v
    maybe
      (fail ("Could not parse FieldTypeScalar from " <> T.unpack t))
      pure
      (preview fieldTypeScalarTextBQ (CI.mk t))

-- I'm not sure why but their API returns INTEGER and FLOAT
-- instead of the documented INT64 and FLOAT64. As such, this side
-- of the prism is a bit "wider", same for BOOLEAN instead of BOOL
extraFieldTypeScalarCases :: M.Map (CI.CI Text) FieldTypeScalar
extraFieldTypeScalarCases =
  M.fromList
    [ ("INTEGER", FieldTypeScalar_Int64),
      ("FLOAT", FieldTypeScalar_Float64),
      ("BOOLEAN", FieldTypeScalar_Bool)
    ]

-- | Prism for parsing and serializing field type scalar to/from bigquery
fieldTypeScalarTextBQ :: Prism' (CI.CI Text) FieldTypeScalar
fieldTypeScalarTextBQ = autoPrismWith extraFieldTypeScalarCases $ \case
  FieldTypeScalar_Int64 -> "INT64"
  FieldTypeScalar_Numeric -> "NUMERIC"
  FieldTypeScalar_Float64 -> "FLOAT64"
  FieldTypeScalar_Bool -> "BOOL"
  FieldTypeScalar_String -> "STRING"
  FieldTypeScalar_Bytes -> "BYTES"
  FieldTypeScalar_Date -> "DATE"
  FieldTypeScalar_DateTime -> "DATETIME"
  FieldTypeScalar_Time -> "TIME"
  FieldTypeScalar_Timestamp -> "TIMESTAMP"
  FieldTypeScalar_JSON -> "JSON"

-- | Prism for parsing and serializing field type scalar for soostone's needs.
-- Specifically we use STRING columns for JSON for some reason. Note taht this
-- means we can't recover FieldTypeScalar_JSON. This probably shouldn't even be
-- a prism but I'm trying not to break anything that isn't already broken.
fieldTypeScalarTextCustom :: Prism' (CI.CI Text) FieldTypeScalar
fieldTypeScalarTextCustom = prism' toT fromT
  where
    toT FieldTypeScalar_Int64 = "INT64"
    toT FieldTypeScalar_Numeric = "NUMERIC"
    toT FieldTypeScalar_Float64 = "FLOAT64"
    toT FieldTypeScalar_Bool = "BOOL"
    toT FieldTypeScalar_String = "STRING"
    toT FieldTypeScalar_Bytes = "BYTES"
    toT FieldTypeScalar_Date = "DATE"
    toT FieldTypeScalar_DateTime = "DATETIME"
    toT FieldTypeScalar_Time = "TIME"
    toT FieldTypeScalar_Timestamp = "TIMESTAMP"
    -- A is encooded as a UTF-8 String
    toT FieldTypeScalar_JSON = "STRING"
    -- I'm not sure why but their API returns INTEGER and FLOAT
    -- instead of the documented INT64 and FLOAT64. As such, this side
    -- of the prism is a bit "wider", same for BOOLEAN instead of BOOL
    fromT "INT64" = Just FieldTypeScalar_Int64
    fromT "INTEGER" = Just FieldTypeScalar_Int64
    fromT "NUMERIC" = Just FieldTypeScalar_Numeric
    fromT "FLOAT64" = Just FieldTypeScalar_Float64
    fromT "FLOAT" = Just FieldTypeScalar_Float64
    fromT "BOOL" = Just FieldTypeScalar_Bool
    fromT "BOOLEAN" = Just FieldTypeScalar_Bool
    fromT "STRING" = Just FieldTypeScalar_String
    fromT "BYTES" = Just FieldTypeScalar_Bytes
    fromT "DATE" = Just FieldTypeScalar_Date
    fromT "DATETIME" = Just FieldTypeScalar_DateTime
    fromT "TIME" = Just FieldTypeScalar_Time
    fromT "TIMESTAMP" = Just FieldTypeScalar_Timestamp
    fromT _ = Nothing

-------------------------------------------------------------------------------

-- | Constructs a schema that's non empty and alphabetizes the columns
mkSchema ::
  NonEmpty (FieldName, FieldSchema) ->
  Schema
mkSchema prs =
  Schema
    { _schema_order = NE.sort (fst <$> prs),
      _schema_fields = M.fromList (toList prs)
    }

-------------------------------------------------------------------------------

-- | Stronger-typed version of bigquery's TableSchema. Does not
-- attempt to be an exhaustive implementation, just as much as we need
-- for our project. Semigroup instance is left-biased
data Schema = Schema
  { -- | N.B. _schema_order columns must exactly match the keys in _schema_fields
    _schema_order :: !(NonEmpty FieldName),
    _schema_fields :: !(M.Map FieldName FieldSchema)
  }
  deriving stock (Show, Eq)

instance A.ToJSON Schema where
  toJSON s =
    A.object
      [ "order" A..= _schema_order s,
        "fields" A..= _schema_fields s
      ]

instance A.FromJSON Schema where
  parseJSON = A.withObject "Schema" $ \o ->
    Schema
      <$> o A..: "order"
      <*> o A..: "fields"

instance AS.SafeJSON Schema where
  version = 0
  kind = AS.base
  safeFrom = AS.containWithObject "Schema" $ \o -> do
    order <- o AS..:$ "order"
    fields <- o AS..:$ "fields"
    pure $
      Schema
        { _schema_order = order,
          _schema_fields = fields
        }
  safeTo schema =
    AS.contain $
      A.object
        [ "order" AS..=$ _schema_order schema,
          "fields" AS..=$ _schema_fields schema
        ]

instance Semigroup Schema where
  (Schema order1 fields1) <> (Schema order2 fields2) =
    Schema (order1 <> order2) (fields1 <> fields2)

toTableSchema :: Schema -> BQ.TableSchema
toTableSchema s = BQ.newTableSchema & #fields ?~ fields_
  where
    fields_ =
      catMaybes
        [mkFieldSchema n <$> M.lookup n (_schema_fields s) | n <- toList (_schema_order s)]

mkFieldSchema :: FieldName -> FieldSchema -> BQ.TableFieldSchema
mkFieldSchema n fs =
  BQ.newTableFieldSchema
    & #mode ?~ CI.original (review fieldModeText (_fieldSchema_mode fs))
    & #name ?~ _fieldName n
    & #type' ?~ typeText (_fieldSchema_type fs)

typeText :: FieldType -> Text
typeText (FieldType_Scalar ty) = CI.original (review fieldTypeScalarTextCustom ty)
-- Arrays encoded as A
typeText (FieldType_Array _) = CI.original (review fieldTypeScalarTextCustom FieldTypeScalar_JSON)

-------------------------------------------------------------------------------

-- | Downgrade our schema type to the gogol-bigquery one
downgradeSchema ::
  HM.HashMap FieldName FieldTypeScalar ->
  HM.HashMap Text BQ.TableFieldSchema
downgradeSchema = HM.fromList . fmap (uncurry convert) . HM.toList
  where
    convert fieldName scalar =
      (fieldNameText', tableFieldSchema')
      where
        fieldNameText' = review fieldNameText fieldName
        tableFieldSchema' =
          BQ.newTableFieldSchema
            & #name ?~ fieldNameText'
            & #type' ?~ CI.original (review fieldTypeScalarTextCustom scalar)

-------------------------------------------------------------------------------

-- | Leniently convert a TableSchema into a Schema, ignoring fields
-- that don't map cleanly. This will only fail when there are no
-- usable columns
fromTableSchemaLenient :: BQ.TableSchema -> Maybe Schema
fromTableSchemaLenient ts = do
  let prs = fromMaybe [] (traverse (hush . convertField) =<< view #fields ts)
  mkSchema <$> nonEmpty prs

convertField :: BQ.TableFieldSchema -> Either Text (FieldName, FieldSchema)
convertField tfs = do
  nameRaw <- note "No field name" (view #name tfs)
  name <- note (nameRaw <> " could not be parsed") (preview fieldNameText nameRaw)
  tyRaw <- note (nameRaw <> " did not have a type") (view #type' tfs)
  ty <- note (nameRaw <> " type " <> tyRaw <> " could not be parsed") (preview fieldTypeScalarTextCustom (CI.mk tyRaw))
  mode <- case view #mode tfs of
    Nothing -> pure FieldMode_Nullable
    Just modeRaw ->
      note (nameRaw <> " mode " <> modeRaw <> " could not be parsed") $
        preview fieldModeText (CI.mk modeRaw)
  let fs =
        FieldSchema
          { _fieldSchema_mode = mode,
            _fieldSchema_type = FieldType_Scalar ty
          }
  pure (name, fs)

-- | Convert a TableSchema into a Schema, accumulating errors and failing on fields that are not usable
fromTableSchema :: BQ.TableSchema -> Either (NonEmpty Text) Schema
fromTableSchema ts = V.toEither $
  case view #fields ts of
    Nothing -> V.validationNel (Left "No fields found")
    Just [] -> V.validationNel (Left "No fields found")
    Just (f : fs) -> mkSchema <$> traverse (V.validationNel . convertField) (f :| fs)

-------------------------------------------------------------------------------
required :: FieldSchema -> FieldSchema
required f = f {_fieldSchema_mode = FieldMode_Required}

nullable :: FieldSchema -> FieldSchema
nullable f = f {_fieldSchema_mode = FieldMode_Nullable}

intField :: FieldSchema
intField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Int64, _fieldSchema_mode = FieldMode_Nullable}

numericField :: FieldSchema
numericField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Numeric, _fieldSchema_mode = FieldMode_Nullable}

floatField :: FieldSchema
floatField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Float64, _fieldSchema_mode = FieldMode_Nullable}

boolField :: FieldSchema
boolField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Bool, _fieldSchema_mode = FieldMode_Nullable}

stringField :: FieldSchema
stringField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_String, _fieldSchema_mode = FieldMode_Nullable}

bytesField :: FieldSchema
bytesField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Bytes, _fieldSchema_mode = FieldMode_Nullable}

dateField :: FieldSchema
dateField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Date, _fieldSchema_mode = FieldMode_Nullable}

dateTimeField :: FieldSchema
dateTimeField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_DateTime, _fieldSchema_mode = FieldMode_Nullable}

timeField :: FieldSchema
timeField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Time, _fieldSchema_mode = FieldMode_Nullable}

timestampField :: FieldSchema
timestampField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_Timestamp, _fieldSchema_mode = FieldMode_Nullable}

jsonField :: FieldSchema
jsonField = FieldSchema {_fieldSchema_type = FieldType_Scalar FieldTypeScalar_JSON, _fieldSchema_mode = FieldMode_Nullable}

arrayOf :: FieldSchema -> FieldSchema
arrayOf s@FieldSchema {_fieldSchema_type = FieldType_Array _} = s
arrayOf s@FieldSchema {_fieldSchema_type = FieldType_Scalar ty} = s {_fieldSchema_type = FieldType_Array ty}

-------------------------------------------------------------------------------

-- | A scalar value that can be parsed using a schema
data Value
  = Value_Int64 Int64
  | Value_Numeric Rational
  | Value_Float64 Double
  | Value_Bool Bool
  | Value_String Text
  | -- | Encodes with base64
    Value_Bytes ByteString
  | Value_Date Day
  | Value_DateTime UTCTime
  | Value_Timestamp BQTimestamp
  | Value_Time Time.TimeOfDay
  | Value_JSON A.Value
  | Value_Null
  deriving stock (Show, Eq, Generic)

instance A.ToJSON Value where
  toJSON (Value_Int64 x) = A.toJSON x
  toJSON (Value_Numeric x) = A.toJSON (fromRational x :: Double)
  toJSON (Value_Float64 x) = A.toJSON x
  toJSON (Value_Bool x) = A.toJSON x
  toJSON (Value_String x) = A.toJSON x
  toJSON (Value_Bytes x) = A.toJSON (SC.toSL (review base64Encoded x) :: Text)
  toJSON (Value_Date x) = A.toJSON (defaultFormatTime bigQueryDateFormatString x)
  toJSON (Value_DateTime x) = A.toJSON (defaultFormatTime bigQueryDateTimeFormatString x)
  toJSON (Value_Timestamp x) = A.toJSON x
  toJSON (Value_Time x) = A.toJSON (defaultFormatTime bigQueryTimeFormatString x)
  toJSON (Value_JSON x) = A.String (SC.toSL (A.encode x))
  toJSON Value_Null = A.Null

-- | Map the field type from the value
valueFieldType :: Value -> Maybe FieldTypeScalar
valueFieldType (Value_Int64 _) = Just FieldTypeScalar_Int64
valueFieldType (Value_Numeric _) = Just FieldTypeScalar_Numeric
valueFieldType (Value_Float64 _) = Just FieldTypeScalar_Float64
valueFieldType (Value_Bool _) = Just FieldTypeScalar_Bool
valueFieldType (Value_String _) = Just FieldTypeScalar_String
valueFieldType (Value_Bytes _) = Just FieldTypeScalar_Bytes
valueFieldType (Value_Date _) = Just FieldTypeScalar_Date
valueFieldType (Value_DateTime _) = Just FieldTypeScalar_DateTime
valueFieldType (Value_Timestamp _) = Just FieldTypeScalar_Timestamp
valueFieldType (Value_Time _) = Just FieldTypeScalar_Time
valueFieldType (Value_JSON _) = Just FieldTypeScalar_JSON
valueFieldType Value_Null = Nothing

-- | Try to parse a value given a type hint
parseValueHint :: FieldTypeScalar -> A.Value -> Either Text Value
parseValueHint _ A.Null = Right Value_Null
parseValueHint FieldTypeScalar_Int64 (A.Number n) = Right (Value_Int64 (truncate n))
parseValueHint FieldTypeScalar_Numeric (A.Number n) = Right (Value_Numeric (toRational n))
parseValueHint FieldTypeScalar_Float64 (A.Number n) = Right (Value_Float64 (realToFrac n))
parseValueHint FieldTypeScalar_Bool (A.Bool b) = Right (Value_Bool b)
parseValueHint FieldTypeScalar_String (A.String s) = Right (Value_String s)
parseValueHint FieldTypeScalar_Bytes (A.String s) = do
  decoded <- note "Invalid base64 in bytes column" (preview base64Encoded (SC.toSL s))
  pure (Value_Bytes decoded)
parseValueHint FieldTypeScalar_Date (A.String t) = do
  d <- note ("Invalid Date " <> t) (defaultParseTimeM bigQueryDateFormatString (SC.toSL t))
  pure (Value_Date d)
parseValueHint FieldTypeScalar_DateTime (A.String t) = do
  d <-
    note
      ("Invalid DateTime " <> t)
      (asum (bigQueryDateTimeFormatStrings <&> \fmt -> defaultParseTimeM fmt (SC.toSL t)))
  pure (Value_DateTime d)
parseValueHint FieldTypeScalar_Time (A.String t) = do
  res <-
    note
      ("Invalid Time " <> t)
      (asum (bigQueryTimeFormatStrings <&> \fmt -> defaultParseTimeM fmt (SC.toSL t)))
  pure (Value_Time res)
parseValueHint FieldTypeScalar_Timestamp (A.String t) = do
  d <-
    note
      ("Invalid Timestamp " <> t)
      (parseBqTimestamp t)
  pure (Value_Timestamp $ BQTimestamp d)
parseValueHint FieldTypeScalar_JSON (A.String s) =
  bimap SC.toS Value_JSON (A.eitherDecode (SC.toSL s))
parseValueHint ty (A.Number _) = Left ("Expected " <> SC.toSL (show ty) <> " got Number")
parseValueHint ty (A.Bool _) = Left ("Expected " <> SC.toSL (show ty) <> " got Bool")
parseValueHint ty (A.String _) = Left ("Expected " <> SC.toSL (show ty) <> " got String")
parseValueHint ty (A.Array _) = Left ("Expected " <> SC.toSL (show ty) <> " got Array")
parseValueHint ty (A.Object _) = Left ("Expected " <> SC.toSL (show ty) <> " got Object")

defaultFormatTime :: Time.FormatTime t => String -> t -> String
defaultFormatTime = Time.formatTime Time.defaultTimeLocale

newtype BQTimestamp = BQTimestamp
  { bqTimestamp :: UTCTime
  }
  deriving stock (Show, Eq)

instance A.ToJSON BQTimestamp where
  toJSON (BQTimestamp x) = A.toJSON $ defaultFormatTime bigQueryTimestampFormatString x

instance A.FromJSON BQTimestamp where
  parseJSON = A.withText "BQTimestamp" $ \text -> case parseBqTimestamp text of
    Nothing -> fail $ "Invalid Timestamp " <> show text
    Just a -> pure $ BQTimestamp a

-------------------------------------------------------------------------------
bigQueryDateFormatString :: String
bigQueryDateFormatString = "%Y-%m-%d"

-- | Default bigquery date format string
bigQueryDateTimeFormatString :: String
bigQueryDateTimeFormatString = "%Y-%m-%dT%H:%M:%S%6Q"

bigQueryDateTimeFormatStrings :: NonEmpty String
bigQueryDateTimeFormatStrings =
  bigQueryDateTimeFormatString
    :| [ "%Y-%m-%dT%H:%M:%S%Q", -- variant without fixed-sized decimal for parsing
         "%Y-%m-%d %H:%M:%S%Q" -- no T
       ]

-- | Default bigquery time format
bigQueryTimeFormatString :: String
bigQueryTimeFormatString = "%H:%M:%S%6Q"

bigQueryTimeFormatStrings :: NonEmpty String
bigQueryTimeFormatStrings =
  bigQueryTimeFormatString
    :| [ "%H:%M:%S%Q" -- variant without fixed-sized decimal for parsing
       ]

-- | Default format of bigquery timestamp
bigQueryTimestampFormatString :: String
bigQueryTimestampFormatString = "%Y-%m-%dT%H:%M:%S%6QZ"

bigQueryTimestampFormatStrings :: NonEmpty String
bigQueryTimestampFormatStrings =
  bigQueryTimestampFormatString
    :| [ "%Y-%m-%dT%H:%M:%S%QZ", -- variant without fixed-sized decimal for parsing
         "%Y-%m-%d %H:%M:%S%QZ", -- no T
         "%Y-%m-%dT%H:%M:%S%Q UTC", -- full zone, if its UTC
         "%Y-%m-%d %H:%M:%S%Q UTC" -- full zone, if its UTC, no T
       ]

-------------------------------------------------------------------------------

-- | Parse time without whitespace, with default locale.
#if MIN_VERSION_time (1, 9, 0)

defaultParseTimeM :: (MonadFail m, Time.ParseTime t) => String -> String -> m t

#else

defaultParseTimeM :: (Monad m, Time.ParseTime t) => String -> String -> m t

#endif
defaultParseTimeM =
  Time.parseTimeM doNotAllowTimeWhitespace Time.defaultTimeLocale
  where
    doNotAllowTimeWhitespace = False

-------------------------------------------------------------------------------
parseBqTimestamp :: Text -> Maybe UTCTime
parseBqTimestamp s =
  asum $
    bigQueryTimestampFormatStrings <&> \fmt ->
      defaultParseTimeM fmt (T.unpack s)

-------------------------------------------------------------------------------
makeLenses ''Schema
makeLenses ''FieldSchema
makePrisms ''Value
