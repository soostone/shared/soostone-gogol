module Soostone.Google.BigQuery
  ( -- * Operations
    createTable,
    createTableIfNotExists,
    tableExists,
    dropTable,
    getTable,
    getTableLastModified,
    countTable,
    syncQuery,
    loadTableSync,
    loadTableAsync,
    insertRows,

    -- ** Polling
    pollJob,
    defaultPollingInterval,

    -- ** Extracts
    FileFormat (..),
    SFileFormat,
    Sing,
    FileFormat_CSVSym0,
    FileFormat_NEWLINE_DELIMITED_JSONSym0,
    extractTableAsync,
    extractTableSync,

    -- * High-level extract
    extractTableToStream,

    -- * Types
    TablePartitioning (..),
    CreateTableOptions (..),
    CreateTableOptions_V0 (..),
    createTableOptions_partitioning,
    createTableOptions_clustering,
    createTableOptions_expirationTime,
    defaultCreateTableOptions,
    dailyPartitioningOn,
    hashedRangePartitioningOn,
    PartitionNumber,
    defaultMaxPartitionNumber,
    partitionNumberInt64,
    mkPartitionNumberTH,
    JobResult (..),
    InsertRow (..),
    insertRow_insertId,
    insertRow_row,
    BigQueryRowObject (..),
    ExtractTableError (..),

    -- * Utilities
    humanReadableTableRef,
    mkTableReference,
    mkTempTableName,
    qualifyTableName,
    sqlQuote,
    quoteFieldName,
    coerceBigQueryRow,
    calculatePartitionIndex,
    calculatePartitionIndexDefault,
  )
where

-------------------------------------------------------------------------------
import Control.Lens.Extended
import Control.Monad.Loops (untilJust)
import qualified Control.Retry as Retry
import qualified Data.Aeson.Extended as A
import Data.Aeson.Lens (_Integral)
import qualified Data.Aeson.Safe as SJ
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Conduit as C
import qualified Data.Conduit.Attoparsec as CA
import qualified Data.Conduit.Combinators as CC
import qualified Data.Conduit.Zlib as CZ
import qualified Data.Digest.Murmur32 as Murmur
import qualified Data.HashMap.Strict as HM
import qualified Data.RNG as RNG
import qualified Data.Scientific as Scientific
import Data.Singletons
import Data.Singletons.TH
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Time as Time
import qualified Data.Time.Clock.POSIX as POSIX
import qualified Data.Time.Locale.Compat as LC
import Data.UUID.Extended (UUID, uuidText)
import qualified Gogol as Google
import qualified Gogol.BigQuery as BQ
import qualified Gogol.Storage as NGCS
import Language.Haskell.TH (Exp, Q)
import Language.Haskell.TH.Syntax (Lift)
import qualified Network.HTTP.Types.Status as NHTS
import qualified Soostone.Google.BigQuery.Schema as BQS
import qualified Soostone.Google.Common as Common
import Soostone.Google.Prelude
import qualified Soostone.Google.Storage as GCS
import Soostone.Google.Types
import qualified System.IO as SIO
import qualified Tempfile

-------------------------------------------------------------------------------

$( singletons
     [d|
       data FileFormat
         = FileFormat_CSV
         | FileFormat_NEWLINE_DELIMITED_JSON
       |]
 )

deriving instance Show FileFormat

deriving instance Eq FileFormat

-------------------------------------------------------------------------------
data CreateTableOptions_V0 = CreateTableOptions_V0
  { _createTableOptions_V0_timePartitioning :: Maybe BQ.TimePartitioning,
    _createTableOptions_V0_clustering :: Maybe (NonEmpty BQS.FieldName)
    -- Optional clustering to be applied at table create time. The
    -- ordering is significant as it indicates the sort order of the
    -- data.
  }
  deriving (Show, Eq)

instance SJ.SafeJSON CreateTableOptions_V0 where
  version = 0
  kind = SJ.base
  safeFrom = SJ.containWithObject "CreateTableOptions_V0" $ \o -> do
    timePartitioning <- o SJ..:? "timePartitioning"
    clustering <- o SJ..:$? "clustering"
    pure $
      CreateTableOptions_V0
        { _createTableOptions_V0_timePartitioning = timePartitioning,
          _createTableOptions_V0_clustering = clustering
        }
  safeTo o =
    SJ.contain $
      SJ.object
        [ "timePartitioning" SJ..= _createTableOptions_V0_timePartitioning o,
          "clustering" SJ..=$ _createTableOptions_V0_clustering o
        ]

-------------------------------------------------------------------------------
data TablePartitioningLabel
  = TablePartitioningLabel_None
  | TablePartitioningLabel_Time
  | TablePartitioningLabel_Range
  deriving stock (Show, Eq, Enum, Bounded, Ord)

instance Universe TablePartitioningLabel

tablePartitioningLabelText :: Prism' Text TablePartitioningLabel
tablePartitioningLabelText = autoPrism $ \case
  TablePartitioningLabel_None -> "NONE"
  TablePartitioningLabel_Time -> "TIME"
  TablePartitioningLabel_Range -> "RANGE"

instance A.ToJSON TablePartitioningLabel where
  toJSON = A.toJSON . review tablePartitioningLabelText

instance A.FromJSON TablePartitioningLabel where
  parseJSON v = do
    t <- A.parseJSON v
    maybe
      (fail ("Could not parse TablePartitioningLabel from " <> T.unpack t))
      pure
      (preview tablePartitioningLabelText t)

-- | The table option to configure partitioning
--
-- Partitioning is the most essential setting to minimize the cost of queries,
-- and there can be only one type of partitioning. If our queries are limited to
-- a date range, using 'TimePartitioning' is the way to go.
--
-- On the other hand, if your main purpose is to access data relevant to a
-- specific identifier, it is better to use 'Range' partitioning. But, Range
-- Partitioning has its own limitations. The maximum number of partitions
-- (currently allowed in BigQuery) is 4000. So, if there is an expectancy that
-- the number of identifiers can be above 4000, it is better to first hash the
-- identifier with an algorithm that uniformly distributes the hash (ie.
-- MurmurHash), scale it down to the configured partition count, and set it as a
-- separate INT64 column to be used as the partitioning column. A helper
-- function (@calculatePartitionIndex@) is included in the module to make this
-- easier. Then, finally use the actual identifier as the initial cluster.
data TablePartitioning
  = TablePartitioning_None
  | TablePartitioning_Time BQ.TimePartitioning
  | TablePartitioning_Range BQ.RangePartitioning
  deriving (Show, Eq, Generic)

instance A.FromJSON TablePartitioning where
  parseJSON = A.withObject "TablePartitioning" $ \o -> do
    o A..: "type" >>= \case
      TablePartitioningLabel_Time -> TablePartitioning_Time <$> o A..: "data"
      TablePartitioningLabel_Range -> TablePartitioning_Range <$> o A..: "data"
      TablePartitioningLabel_None -> pure TablePartitioning_None

instance A.ToJSON TablePartitioning where
  toJSON o = A.object $ case o of
    TablePartitioning_Time p -> ["type" A..= TablePartitioningLabel_Time, "data" A..= p]
    TablePartitioning_Range p -> ["type" A..= TablePartitioningLabel_Range, "data" A..= p]
    TablePartitioning_None -> ["type" A..= TablePartitioningLabel_None]

data CreateTableOptions = CreateTableOptions
  { _createTableOptions_partitioning :: TablePartitioning,
    _createTableOptions_clustering :: Maybe (NonEmpty BQS.FieldName),
    _createTableOptions_expirationTime :: Maybe UTCTime
    -- Optional clustering to be applied at table create time. The
    -- ordering is significant as it indicates the sort order of the
    -- data.
  }
  deriving (Show, Eq)

instance SJ.Migrate CreateTableOptions where
  type MigrateFrom CreateTableOptions = CreateTableOptions_V0
  migrate ctoV0 =
    CreateTableOptions
      { _createTableOptions_partitioning = maybe TablePartitioning_None TablePartitioning_Time (_createTableOptions_V0_timePartitioning ctoV0),
        _createTableOptions_clustering = _createTableOptions_V0_clustering ctoV0,
        _createTableOptions_expirationTime = Nothing
      }

instance SJ.SafeJSON CreateTableOptions where
  version = 1
  kind = SJ.extension
  safeFrom = SJ.containWithObject "CreateTableOptions" $ \o -> do
    tablePartitioning <- o SJ..: "partitioning"
    clustering <- o SJ..:$? "clustering"
    expirationTime <- o SJ..:$? "expirationTime"
    pure $
      CreateTableOptions
        { _createTableOptions_partitioning = tablePartitioning,
          _createTableOptions_clustering = clustering,
          _createTableOptions_expirationTime = expirationTime
        }
  safeTo o =
    SJ.contain $
      SJ.object
        [ "partitioning" SJ..= _createTableOptions_partitioning o,
          "clustering" SJ..=$ _createTableOptions_clustering o,
          "expirationTime" SJ..=$ _createTableOptions_expirationTime o
        ]

defaultCreateTableOptions :: CreateTableOptions
defaultCreateTableOptions =
  CreateTableOptions
    { _createTableOptions_partitioning = TablePartitioning_None,
      _createTableOptions_clustering = Nothing,
      _createTableOptions_expirationTime = Nothing
    }

-------------------------------------------------------------------------------

newtype PartitionNumber = PartitionNumber
  { partitionNumber :: Int64
  }
  deriving stock (Show, Eq, Lift)
  deriving newtype (Ord)

instance Bounded PartitionNumber where
  minBound = PartitionNumber 1
  maxBound = PartitionNumber 4000

-- | The maximum number of partitions that is allowed in BigQuery is 4000
defaultMaxPartitionNumber :: PartitionNumber
defaultMaxPartitionNumber = maxBound

-- | A prism that makes sure the value we have is in the acceptable range of partition counts
partitionNumberInt64 :: Prism' Int64 PartitionNumber
partitionNumberInt64 = prism' partitionNumber $ \num ->
  if num >= partitionNumber minBound && num <= partitionNumber maxBound
    then Just (PartitionNumber num)
    else Nothing

-- | Helper TH function to ensure getting a valid PartitionNumber at compile time: @$(mkPartitionNumberTH 4000)@
mkPartitionNumberTH :: Int64 -> Q Exp
mkPartitionNumberTH num =
  previewTH
    ("Expected a valid PartitionNumber (in range 1-4000), received: " <> show num)
    partitionNumberInt64
    num

-- | Sets up time partitioning to partition daily on the given
-- column. This feature is currently in beta in bigquery as of 08/2018
dailyPartitioningOn :: BQS.FieldName -> BQ.TimePartitioning
dailyPartitioningOn fieldName =
  BQ.newTimePartitioning
    & #field ?~ review BQS.fieldNameText fieldName
    & #type' ?~ "DAY"

-- | Sets up range partitioning to an INT64 field that is in range 1-4000 (which
-- can be used as a hash range via a helper function)
--
-- NOTE: The maximum number of partitions allowed in BigQuery is 4000.
--
-- NOTE: Range indices start with '1'
--
-- NOTE: The value of @fieldName@ must be calculated with the @prx@ in mind, and
-- cannot exceed its value.
hashedRangePartitioningOn :: PartitionNumber -> BQS.FieldName -> BQ.RangePartitioning
hashedRangePartitioningOn prx fieldName =
  BQ.newRangePartitioning
    & #field ?~ review BQS.fieldNameText fieldName
    & #range
      ?~ ( BQ.newRangePartitioning_Range
             & #start ?~ fromIntegral (partitionNumber (minBound @PartitionNumber))
             & #end ?~ fromIntegral (partitionNumber prx)
             & #interval ?~ 1
         )

-- | calculates a reproducable partition index for a given ByteString while also
-- trying to span the index to the provided total partition count. The hard
-- upper limit for the number of partitions for BigQuery is 4000. If, the number
-- of unique values of identifier column this index is being calculated from is
-- expected to be smaller, it may be more ideal to use smaller values for
-- @PartitionNumber@.
--
-- NOTE: the indices start with '1'
calculatePartitionIndex :: PartitionNumber -> ByteString -> PartitionNumber
calculatePartitionIndex (PartitionNumber prx) =
  toPartition
    . Murmur.asWord32
    . Murmur.hash32
  where
    toPartition w = PartitionNumber (1 + fromIntegral (w `mod` fromIntegral prx))

-- | A variant of `calculatePartitionIndex` with the maximum number of partitions allowed
calculatePartitionIndexDefault :: ByteString -> PartitionNumber
calculatePartitionIndexDefault = calculatePartitionIndex defaultMaxPartitionNumber

-------------------------------------------------------------------------------
-- TODO: consider throwing?
createTableIfNotExists ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesGet scopes,
    Google.AllowRequest BQ.BigQueryTablesInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  -- | Our safe variant of bigquery schemas
  BQS.Schema ->
  CreateTableOptions ->
  m (Either Google.Error BQ.Table)
createTableIfNotExists retryPolicy env projId dsetId table schema opts = runExceptT $ do
  exists <- lift (tableExists retryPolicy env projId dsetId table)
  if exists
    then ExceptT (getTable retryPolicy env projId dsetId table)
    else ExceptT (createTable retryPolicy env projId dsetId table schema opts)

-------------------------------------------------------------------------------
tableExists ::
  ( MonadMask m,
    MonadUnliftIO m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  m Bool
tableExists retryPolicy env projId dsetId tableId = do
  res <- getTable retryPolicy env projId dsetId tableId
  case res of
    Right _ -> pure True
    Left e
      | (NHTS.statusCode <$> Common.geExceptionStatus e) == Just 404 -> pure False
      | otherwise -> throwM e

-------------------------------------------------------------------------------

-- | Deletes a table. If the table does not exist, this is a noop.
dropTable ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesDelete scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  m (Either Google.Error ())
dropTable retryPolicy env projId dsetId tableId =
  try (catchJust catch404 (Common.runGoogle retryPolicy (Google.send env cmd)) return)
  where
    -- Weird argument ordering...
    cmd =
      BQ.newBigQueryTablesDelete
        (review bqDatasetIdText dsetId)
        (review bqProjectIdText projId)
        (review bqTableIdText tableId)
    catch404 googleError = case NHTS.statusCode <$> Common.geExceptionStatus googleError of
      Just 404 -> Just ()
      _ -> Nothing

-------------------------------------------------------------------------------
getTable ::
  ( MonadMask m,
    MonadUnliftIO m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  m (Either Google.Error BQ.Table)
getTable retryPolicy env pid dsid table =
  Common.runGoogle retryPolicy (try (Google.send env query))
  where
    query =
      BQ.newBigQueryTablesGet
        (review bqDatasetIdText dsid)
        (review bqProjectIdText pid)
        (review bqTableIdText table)

-------------------------------------------------------------------------------
createTable ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  BQS.Schema ->
  CreateTableOptions ->
  m (Either Google.Error BQ.Table)
createTable retryPolicy env projId dsetId tableId schema =
  createTable' retryPolicy env projId dsetId tableId (BQS.toTableSchema schema)

-- | Lowest level create table that uses the native bigquery schema
-- type. Not exported due to the error-prone nature of TableSchema.
createTable' ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  -- | Stringly-typed schema.
  BQ.TableSchema ->
  CreateTableOptions ->
  m (Either Google.Error BQ.Table)
createTable' retryPolicy env projId dsetId table schema opts = do
  $(logTM) DebugS ("Creating BigQuery table " <> ls (humanReadableTableRef projId dsetId table))
  try (Common.runGoogle retryPolicy (Google.send env cmd))
  where
    tref = mkTableReference projId dsetId table
    partitioning (x :: BQ.Table) = case _createTableOptions_partitioning opts of
      TablePartitioning_None -> x
      TablePartitioning_Time popts -> x & #timePartitioning ?~ popts
      TablePartitioning_Range popts -> x & #rangePartitioning ?~ popts
    -- The time when this table expires, in milliseconds since the epoch
    expirationTimeMay =
      floor
      . (1e3 *)
      . Time.nominalDiffTimeToSeconds
      . POSIX.utcTimeToPOSIXSeconds
      <$> _createTableOptions_expirationTime opts
    tbl =
      BQ.newTable
        & #tableReference ?~ tref
        & #schema ?~ schema
        & #clustering .~ (_createTableOptions_clustering opts <&> \fields -> BQ.newClustering & #fields ?~ (review BQS.fieldNameText <$> toList fields))
        & partitioning
        & #expirationTime .~ expirationTimeMay
    cmd =
      BQ.newBigQueryTablesInsert
        (review bqDatasetIdText dsetId)
        tbl
        (review bqProjectIdText projId)

-------------------------------------------------------------------------------

-- | Get the last modified timestamp of a table. Useful for determining if a table has changed
getTableLastModified ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryTablesGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  m (Either Google.Error (Maybe UTCTime))
getTableLastModified retryPolicy env pid dsid table = runExceptT $ do
  tbl <- ExceptT (getTable retryPolicy env pid dsid table)
  pure (POSIX.posixSecondsToUTCTime . posixMilliseconds . Milliseconds . fromIntegral <$> view #lastModifiedTime tbl)

-------------------------------------------------------------------------------

-- | Human-readable, fully-qualified reference to a table in the
-- format of @projectid.datasetid.tablename@. Useful for logging
-- purposes.
humanReadableTableRef :: BQProjectId -> BQDatasetId -> BQTableId -> Text
humanReadableTableRef projId dsetId tableName =
  review bqProjectIdText projId
    <> "."
    <> review bqDatasetIdText dsetId
    <> "."
    <> review bqTableIdText tableName

mkTableReference ::
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  BQ.TableReference
mkTableReference projId dsetId tableName =
  BQ.newTableReference
    & #datasetId ?~ review bqDatasetIdText dsetId
    & #projectId ?~ review bqProjectIdText projId
    & #tableId ?~ review bqTableIdText tableName

-------------------------------------------------------------------------------
sendJob ::
  ( MonadMask m,
    MonadUnliftIO m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryJobsInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQ.BigQueryJobsInsert ->
  m (BQ.JobReference, BQ.Job)
sendJob retryPolicy env j = do
  res <- Common.runGoogle retryPolicy (Google.send env j)
  -- gogol is unfortunately very partial. Job reference is always
  -- present in this case, so we corral all the partiality to this
  -- function.
  let jr = fromMaybe (error "Impossible! sendJob job reference missing.") (view #jobReference res)
  return (jr, res)

-------------------------------------------------------------------------------
pollJob ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryJobsGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  Seconds ->
  BQProjectId ->
  BQ.JobReference ->
  m JobResult
pollJob retryPolicy env secs projId jr = untilJust $ do
  jobStatus <- Common.runGoogle retryPolicy (Google.send env jg)
  stat <-
    maybe
      (error "Impossible, job did not have status")
      pure
      (jobStatus ^. #status)
  case (stat ^. #state, stat ^. #errorResult, stat ^. #errors) of
    (Just "DONE", Nothing, Nothing) -> return (Just JobResult_Successful)
    (Just "DONE", Nothing, Just []) -> return (Just JobResult_Successful)
    (Just "DONE", Nothing, Just (e : es)) -> return (Just (JobResult_PartiallySuccessful (e :| es)))
    (Just "DONE", Just fatalError, otherErrors) -> return (Just (JobResult_TotalFailure fatalError (fromMaybe [] otherErrors)))
    _ -> do
      liftIO (threadDelay (secondsToMicros secs))
      return Nothing
  where
    jg = BQ.newBigQueryJobsGet jid (review bqProjectIdText projId)
    jid = fromMaybe (error "Impossible: job reference without id") (view #jobId jr)

-------------------------------------------------------------------------------
countTable ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryJobsQuery scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  m (Either Google.Error Int)
countTable retryPolicy env pid dsid tid = runExceptT $ do
  resp <-
    ExceptT $
      syncQuery
        retryPolicy
        env
        pid
        dsid
        query
  pure (fromMaybe (0 :: Int) (resp ^? #rows . _Just . ix 0 . #f . _Just . ix 0 . #v . _Just . _Integral))
  where
    query = BQStandardSQLQuery ("SELECT COUNT(*) FROM " <> review bqTableIdText tid <> ";")

------------------------------------------------------------------------------

-- | Issue a query synchronously and retrieve the response
syncQuery ::
  ( MonadUnliftIO m,
    MonadMask m,
    KatipContext m,
    Google.AllowRequest BQ.BigQueryJobsQuery scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQStandardSQLQuery ->
  m (Either Google.Error BQ.QueryResponse)
syncQuery retryPolicy env pid dsid q = do
  $(logTM) DebugS ("Running BQ query: " <> ls (bqStandardSQLQuery q))
  try (Common.runGoogle retryPolicy (Google.send env jobsQuery))
  where
    dataSet =
      BQ.newDatasetReference
        & #datasetId ?~ review bqDatasetIdText dsid
        & #projectId ?~ review bqProjectIdText pid
    queryRequest =
      BQ.newQueryRequest
        & #query ?~ bqStandardSQLQuery q
        & #useLegacySql .~ False
        & #defaultDataset ?~ dataSet
        & #useQueryCache .~ False -- Never want to cache commands
    jobsQuery = BQ.newBigQueryJobsQuery queryRequest (review bqProjectIdText pid)

-------------------------------------------------------------------------------
data JobResult
  = JobResult_Successful
  | JobResult_PartiallySuccessful (NonEmpty BQ.ErrorProto)
  | JobResult_TotalFailure
      BQ.ErrorProto
      -- Fatal error
      [BQ.ErrorProto]
  -- Additional errors
  deriving (Show, Eq)

-------------------------------------------------------------------------------

-- | Start a table load (append) given a list of CSV files uploaded to GCS. Preconditions:
--
-- 1. The table must already exist and have a schema.
-- 2. For CSV files, they are assumed to be without headers and so columns must be in the schema order.
-- 3. There are fewer than 10 million files being provided <https://cloud.google.com/bigquery/quotas#load_jobs>
--
-- Returns immediately with job reference which can be monitored for
-- completion.
loadTableAsync ::
  forall m fileFormat scopes.
  ( MonadMask m,
    MonadUnliftIO m,
    KatipContext m,
    SingI fileFormat,
    Google.AllowRequest BQ.BigQueryJobsInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  Tagged (Sing (fileFormat :: FileFormat)) (NonEmpty (GCSBucketName, GCSKey)) ->
  LoadBQTableOptions ->
  m BQ.JobReference
loadTableAsync retryPolicy env projId dsetId tableName files opts = do
  $(logTM) DebugS ("Loading BigQuery table " <> ls (humanReadableTableRef projId dsetId tableName) <> " with " <> showLS fileCount <> " " <> ls formatHumanFriendly <> " files")
  fst <$> sendJob retryPolicy env jlta
  where
    format = fromSing (sing @fileFormat)
    formatHumanFriendly :: Text
    formatHumanFriendly = case format of
      FileFormat_CSV -> "CSV"
      FileFormat_NEWLINE_DELIMITED_JSON -> "Newline delimited JSON"
    fileCount = length (untag files)
    uris = uncurry GCS.humanReadableGCSURL' <$> untag files
    tbl = mkTableReference projId dsetId tableName
    noLeadingRows = 0
    load =
      BQ.newJobConfigurationLoad
        & #sourceUris ?~ toList uris
        & #destinationTable ?~ tbl
        & #skipLeadingRows .~ case format of
          FileFormat_CSV -> Just noLeadingRows
          FileFormat_NEWLINE_DELIMITED_JSON -> Nothing
        & #writeDisposition ?~ "WRITE_APPEND"
        & #allowJaggedRows .~ case format of
          FileFormat_CSV -> Just False
          FileFormat_NEWLINE_DELIMITED_JSON -> Nothing
        & #ignoreUnknownValues ?~ False
        & #createDisposition ?~ "CREATE_NEVER"
        & #allowQuotedNewlines .~ case format of
          FileFormat_CSV -> Just True
          FileFormat_NEWLINE_DELIMITED_JSON -> Nothing
        & #sourceFormat ?~ case format of
          FileFormat_CSV -> "CSV"
          FileFormat_NEWLINE_DELIMITED_JSON -> "NEWLINE_DELIMITED_JSON"
        & #maxBadRecords ?~ fromIntegral (_loadBQTableOptions_maxBadRecords opts)
        & #autodetect ?~ False
        & #encoding ?~ "UTF-8"
    jcfg = BQ.newJobConfiguration & #load ?~ load
    job = BQ.newJob & #configuration ?~ jcfg
    jlta = BQ.newBigQueryJobsInsert job (review bqProjectIdText projId)

-------------------------------------------------------------------------------
loadTableSync ::
  ( MonadMask m,
    MonadUnliftIO m,
    KatipContext m,
    SingI fileFormat,
    Google.AllowRequest BQ.BigQueryJobsInsert scopes,
    Google.AllowRequest BQ.BigQueryJobsGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  Tagged (Sing (fileFormat :: FileFormat)) (NonEmpty (GCSBucketName, GCSKey)) ->
  LoadBQTableOptions ->
  -- | Job polling interval
  Seconds ->
  m JobResult
loadTableSync retryPolicy env bqpid bqdsid bqtn files opts pollingInterval = do
  jr <- loadTableAsync retryPolicy env bqpid bqdsid bqtn files opts
  pollJob retryPolicy env pollingInterval bqpid jr

-------------------------------------------------------------------------------

-- | Note that this insert might not be committed by the time this
-- returns, even when successful.
insertRows ::
  ( KatipContext m,
    MonadUnliftIO m,
    MonadMask m,
    HasError Google.Error es,
    HasError (NonEmpty BQ.TableDataInsertAllResponse_InsertErrorsItem) es,
    Google.AllowRequest BQ.BigQueryTabledataInsertAll scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  NonEmpty InsertRow ->
  m (Either (Errors es) ())
insertRows retryPolicy env pid dsid table rows = do
  res <-
    try $
      Common.runGoogle retryPolicy $
        Google.send env $
          BQ.newBigQueryTabledataInsertAll
            (review bqDatasetIdText dsid)
            payload
            (review bqProjectIdText pid)
            (review bqTableIdText table)
  pure $ case res of
    Left ex -> Left (liftError (ex :: Google.Error))
    Right resp -> case view #insertErrors resp of
      Just (e : es) -> Left (liftError (e :| es))
      _ -> Right ()
  where
    payload =
      BQ.newTableDataInsertAllRequest
        & #rows ?~ toList (toRow <$> rows)
    toRow ins =
      BQ.newTableDataInsertAllRequest_RowsItem
        & #json ?~ BQ.newJsonObject (A.toHashMapText (_insertRow_row ins))
        & #insertId .~ _insertRow_insertId ins

data InsertRow = InsertRow
  { _insertRow_row :: A.Object,
    -- | Optional unique ID for each row which bigquery will make a "best effort" to detect duplicate inserts
    _insertRow_insertId :: Maybe Text
  }
  deriving (Show, Eq)

-------------------------------------------------------------------------------

-- | Extract a full table to a set of gzip-compressed files in google
-- storage in the specified format. The prefix should be distinctive
-- as you probably intend to scan the files once extracted. The given
-- format is remembered at the type level. You are encoraged to use
-- the functor/applicative/monad instances of the tagged prefix to
-- retain the format until the point at which you extract and parse
-- it.
extractTableAsync ::
  ( MonadUnliftIO m,
    KatipContext m,
    MonadMask m,
    Google.AllowRequest BQ.BigQueryJobsInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  GCSBucketName ->
  GCSPrefix ->
  Sing (extractFormat :: FileFormat) ->
  -- e.g. SFileFormat_CSV or SFileFormat_NEWLINE_DELIMITED_JSON
  m (Tagged (Sing "GZIP", Sing extractFormat) BQ.JobReference)
extractTableAsync retryPolicy env projId dsetId tableName bucketName prefix extractFormat = do
  $(logTM) DebugS ("Extracting BigQueryTable " <> ls (humanReadableTableRef projId dsetId tableName) <> " to " <> ls destination)
  jobRef <- fst <$> sendJob retryPolicy env jobIns
  pure (Tagged jobRef)
  where
    jobIns = BQ.newBigQueryJobsInsert job (review bqProjectIdText projId)
    job = BQ.newJob & #configuration ?~ jcfg
    jcfg = BQ.newJobConfiguration & #extract ?~ extract
    destination = GCS.humanReadableGCSURL (GCSFile bucketName (GCSPath (_gcsPrefix prefix </> $(mkRelFile "*.gz"))))
    tbl = mkTableReference projId dsetId tableName
    extract =
      BQ.newJobConfigurationExtract
        & #destinationFormat ?~ case fromSing extractFormat of
          FileFormat_CSV -> "CSV"
          FileFormat_NEWLINE_DELIMITED_JSON -> "NEWLINE_DELIMITED_JSON"
        & #sourceTable ?~ tbl
        & #compression ?~ "GZIP"
        & #destinationUris ?~ [destination]

extractTableSync ::
  ( MonadUnliftIO m,
    KatipContext m,
    MonadMask m,
    Google.AllowRequest BQ.BigQueryJobsInsert scopes,
    Google.AllowRequest BQ.BigQueryJobsGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  GCSBucketName ->
  GCSPrefix ->
  Sing (extractFormat :: FileFormat) ->
  -- e.g. SFileFormat_CSV or SFileFormat_NEWLINE_DELIMITED_JSON

  -- | Polling interval
  Seconds ->
  m (Tagged (Sing "GZIP", Sing extractFormat) JobResult)
extractTableSync retryPolicy env projId dsetId tableName bucketName prefix extractFormat pollingInterval = do
  jobRef <- extractTableAsync retryPolicy env projId dsetId tableName bucketName prefix extractFormat
  jobRes <- pollJob retryPolicy env pollingInterval projId (unTagged jobRef)
  pure (jobRes <$ jobRef)

-------------------------------------------------------------------------------
mkTempTableName ::
  (Monad m) =>
  m UUID ->
  -- Prefix for the table to differentiate it
  BQTableId ->
  m (Maybe BQTableId)
mkTempTableName genUUID pfx = do
  uuid <- genUUID
  pure (preview bqTableIdText (review bqTableIdText pfx <> T.replace "-" "_" (review uuidText uuid)))

-------------------------------------------------------------------------------

-- | This wraps JSON objects but is a reminder that bigquery rows work
-- rather counter-intuitively. For example, all numbers are rendered
-- as strings instead. We can use the table schema to coerce these
-- values to something more reasonable.
newtype BigQueryRowObject
  = BigQueryRowObject A.Object
  deriving (Show, Eq)

-- | Make a best effort to convert a bigquery's peculiar JSON format
-- into a richer type. The first argument is in a hashmap for
-- efficiency's sake since you'll likely be running this in a tight
-- loop. Use 'hashTableSchema' to build that value. It appears that
-- bigquery will export "jagged" rows, omitting columns on a
-- row-by-row basis if they are null. This function will pad those to
-- null values. It will also omit any values not in the schema (which
-- shouldn't normally be the case when the schema is retrieved directly
-- from BQ). Any fields in the schema which are not valid as field
-- names (should be impossible) will be omitted as well. It only takes
-- a raw Text key to make it easy to use with the schema you get from
-- BigQuery.
coerceBigQueryRow ::
  -- | Field schema
  HM.HashMap Text BQ.TableFieldSchema ->
  BigQueryRowObject ->
  HM.HashMap BQS.FieldName BQS.Value
coerceBigQueryRow schema (BigQueryRowObject obj) = HM.mapWithKey coerceValue safeSchema
  where
    safeSchema =
      HM.fromList $
        catMaybes $
          HM.toList schema <&> \(k, t) -> do
            fieldName <- preview BQS.fieldNameText k
            pure (fieldName, t)
    coerceValue fieldName fieldSchema =
      maybe
        BQS.Value_Null
        (coerceField fieldSchema)
        (HM.lookup (review BQS.fieldNameText fieldName) (A.toHashMapText obj))

-- | Make a best effort to coerce a single field using that field's schema
coerceField ::
  BQ.TableFieldSchema ->
  A.Value ->
  BQS.Value
coerceField schema val = fromMaybe defMapping $ do
  ty <- view #type' schema
  case val of
    A.String txt -> case ty of
      "STRING" -> tryDecodeJSON txt <|> Just (BQS.Value_String txt)
      "INT64" -> BQS.Value_Int64 <$> readMay (T.unpack txt)
      "INTEGER" -> BQS.Value_Int64 <$> readMay (T.unpack txt)
      "NUMERIC" -> BQS.Value_Numeric . toRational @Double <$> readMay (T.unpack txt)
      "FLOAT64" -> BQS.Value_Float64 <$> readMay (T.unpack txt)
      "BYTES" -> BQS.Value_Bytes <$> preview base64Encoded (T.encodeUtf8 txt)
      "DATE" -> BQS.Value_Date <$> Time.parseTimeM doNotAcceptWhitespace locale BQS.bigQueryDateFormatString (T.unpack txt)
      "DATETIME" -> BQS.Value_DateTime <$> asum (BQS.bigQueryDateTimeFormatStrings <&> \fmt -> Time.parseTimeM doNotAcceptWhitespace locale fmt (T.unpack txt))
      "TIMESTAMP" -> BQS.Value_Timestamp . BQS.BQTimestamp <$> BQS.parseBqTimestamp txt
      "TIME" -> BQS.Value_Time <$> asum (BQS.bigQueryTimeFormatStrings <&> \fmt -> Time.parseTimeM doNotAcceptWhitespace locale fmt (T.unpack txt))
      _ -> Nothing
    _ -> Nothing
  where
    locale = LC.defaultTimeLocale
    doNotAcceptWhitespace = False
    tryDecodeJSON txt = A.decode (BSL.fromStrict (T.encodeUtf8 txt)) >>= \case
      o@(A.Object _) -> Just (BQS.Value_JSON o)
      a@(A.Array _) -> Just (BQS.Value_JSON a)
      _ -> Nothing
    defMapping = case val of
      A.String txt -> fromMaybe (BQS.Value_String txt) (tryDecodeJSON txt)
      A.Number sci -> case Scientific.floatingOrInteger sci of
        Left dbl -> BQS.Value_Float64 dbl
        Right i64 -> BQS.Value_Int64 i64
      A.Bool b -> BQS.Value_Bool b
      A.Object _ -> BQS.Value_JSON val
      A.Array _ -> BQS.Value_JSON val
      A.Null -> BQS.Value_Null

hashTableSchema :: BQ.TableSchema -> HM.HashMap Text BQ.TableFieldSchema
hashTableSchema = HM.fromList . mapMaybe mkPair . fromMaybe [] . view #fields
  where
    mkPair fieldSchema = do
      name <- view #name fieldSchema
      pure (name, fieldSchema)

-------------------------------------------------------------------------------

-- | Typed conduit that takes type-tagged bytes which are gzipped and in the newline delimited JSON format and safely decodes them
extractCompressedJSONStream ::
  ( PrimMonad m,
    MonadThrow m
  ) =>
  C.ConduitM (Tagged (Sing "GZIP", Sing 'FileFormat_NEWLINE_DELIMITED_JSON) ByteString) (Either CA.ParseError A.Value) m ()
extractCompressedJSONStream =
  CC.map unTagged
    C..| CZ.ungzip
    C..| A.parseJSONStream

-------------------------------------------------------------------------------
qualifyTableName :: BQDatasetId -> BQTableId -> Text
qualifyTableName dsid tid =
  sqlQuote (review bqDatasetIdText dsid)
    <> "."
    <> sqlQuote (review bqTableIdText tid)

sqlQuote :: Text -> Text
sqlQuote t = "`" <> t <> "`"

quoteFieldName :: BQS.FieldName -> Text
quoteFieldName = sqlQuote . review BQS.fieldNameText

-------------------------------------------------------------------------------
data ExtractTableError
  = ExtractTableError_TableLookupError Google.Error
  | ExtractTableError_NoSchema BQProjectId BQDatasetId BQTableId
  | ExtractTableError_ExtractErrors (NonEmpty BQ.ErrorProto)
  deriving (Show)

-- | Given a table and a prefix, extract the table into compressed
-- files. From there, stream each file one by one to the disk and from
-- there, stream each row from the file through a conduit as parsed
-- JSON. On the way, the temporary files in google storage and on disk
-- are cleaned up via ResourceT
extractTableToStream ::
  forall m scopes.
  ( MonadUnliftIO m,
    KatipContext m,
    MonadMask m,
    PrimMonad m,
    Google.AllowRequest BQ.BigQueryTablesGet scopes,
    Google.AllowRequest BQ.BigQueryJobsInsert scopes,
    Google.AllowRequest BQ.BigQueryJobsGet scopes,
    Google.AllowRequest NGCS.StorageObjectsDelete scopes,
    Google.AllowRequest NGCS.StorageObjectsGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  RNG.RNG ->
  -- | Temp dir
  Path Abs Dir ->
  Google.Env scopes ->
  BQProjectId ->
  BQDatasetId ->
  BQTableId ->
  GCSBucketName ->
  GCSPrefix ->
  -- | Polling interval
  Seconds ->
  m (Either ExtractTableError (C.ConduitM () (HM.HashMap BQS.FieldName BQS.Value) (ResourceT m) ()))
extractTableToStream retryPolicy rng tmpDir googleEnv projId dsetId tableName bucketName prefix pollingInterval = runExceptT $
  katipAddContext baseCtx $ do
    table <-
      fmapLT ExtractTableError_TableLookupError $
        ExceptT $
          getTable retryPolicy googleEnv projId dsetId tableName
    tableSchema <-
      noteT (ExtractTableError_NoSchema projId dsetId tableName) $
        hoistMaybe $
          view #schema table
    let schema = hashTableSchema tableSchema

    $(logTM) InfoS "Extracting table to gcs"
    result <-
      lift $
        extractTableSync
          retryPolicy
          googleEnv
          projId
          dsetId
          tableName
          bucketName
          prefix
          SFileFormat_NEWLINE_DELIMITED_JSON
          pollingInterval

    case result of
      (Tagged (JobResult_PartiallySuccessful ers)) ->
        throwE (ExtractTableError_ExtractErrors ers)
      (Tagged (JobResult_TotalFailure fatalError additionalErrors)) ->
        throwE (ExtractTableError_ExtractErrors (fatalError :| additionalErrors))
      (Tagged JobResult_Successful) -> do
        $(logTM) InfoS "Table extracted. Enumerating files."
        pure $
          streamGCSFiles
            C..|
            -- For each object, extract the key, discard if missing, and tag with file info
            CC.concatMap (\obj -> view #name obj <&> \rawKey -> GCSKey rawKey <$ result)
            C..|
            -- Register a cleanup action for each one so we never leak anything
            CC.mapM
              ( \taggedKey@(Tagged rawKey) -> do
                  runInIO <- lift askRunInIO
                  releaseKey <- register (runInIO (runResourceT (GCS.deleteObject retryPolicy googleEnv bucketName rawKey)))
                  pure ((releaseKey,) <$> taggedKey)
              )
            C..| streamFile schema
  where
    streamGCSFiles :: C.ConduitM () NGCS.Object (ResourceT m) ()
    streamGCSFiles =
      GCS.streamObjectsInPrefix
        (Retry.natTransformRetryPolicy lift retryPolicy)
        googleEnv
        bucketName
        prefix
    -- We operate in the ResourceT monad to be sure we clean things up
    -- but to keep disk and GCS usage low, we clean up as soon as
    -- possible within the loop.
    streamFile ::
      HM.HashMap Text BQ.TableFieldSchema ->
      C.ConduitM (Tagged (Sing "GZIP", Sing 'FileFormat_NEWLINE_DELIMITED_JSON) (ReleaseKey, GCSKey)) (HM.HashMap BQS.FieldName BQS.Value) (ResourceT m) ()
    streamFile schema = C.awaitForever $ \keyTagged -> do
      let (releaseGCSFile, key) = unTagged keyTagged
      -- we need to do this because we don't have a katip context instance for conduit
      let writeLog sev msg = lift (katipAddContext (sl "gcs_key" key) ($(logTM) sev msg))
      writeLog DebugS ("Creating tempfile for " <> ls (_gcsKey key))
      (releaseTempFile, tempFile, fileHandle) <- lift $ Tempfile.mkTempFileRT rng tmpDir $(mkRelFile "extract.json")
      -- We aren't writing to the file handle
      liftIO (SIO.hClose fileHandle)

      writeLog DebugS ("Downloading tempfile from " <> ls (_gcsKey key) <> " to " <> ls (toFilePath tempFile))
      lift $ GCS.downloadObjectToFile (Retry.natTransformRetryPolicy lift retryPolicy) googleEnv bucketName key tempFile

      -- we don't need the GCSfile anymore
      writeLog DebugS ("Deleting object at " <> ls (_gcsKey key))
      lift $ release releaseGCSFile

      writeLog DebugS ("Streaming locally cached file " <> ls (toFilePath tempFile))
      -- For each item, we're opening a stream to the file and
      -- yielding to the outer conduit each item we parse successfully
      C.runConduit $ do
        CC.sourceFile (toFilePath tempFile)
          C..|
          -- Impart the type tag from the key to each chunk of the file
          CC.map (<$ keyTagged)
          C..| extractCompressedJSONStream
          C..| CC.mapM_
            ( \case
                Left parseError ->
                  lift $
                    $(logTM) ErrorS ("Parse error while streaming extract: " <> showLS parseError)
                Right (A.Object obj) -> C.yield (coerceBigQueryRow schema (BigQueryRowObject obj))
                Right x ->
                  lift $
                    $(logTM) WarningS ("Encountered a non-object in an extracted stream. This should not be possible. " <> showLS x)
            )

      writeLog DebugS ("File " <> ls (toFilePath tempFile) <> " finished. Cleaning it up.")
      -- We don't need the local temp file anymore either
      lift $ release releaseTempFile
    baseCtx =
      sl "project_id" (review bqProjectIdText projId)
        <> sl "dataset_id" (review bqDatasetIdText dsetId)
        <> sl "table_id" (review bqTableIdText tableName)
        <> sl "gcs_bucket" (review gcsBucketNameText bucketName)
        <> sl "gcs_prefix" (_gcsPrefix prefix)

-------------------------------------------------------------------------------
defaultPollingInterval :: Seconds
defaultPollingInterval = Seconds 10

-------------------------------------------------------------------------------
makeLenses ''CreateTableOptions

makeLenses ''InsertRow
