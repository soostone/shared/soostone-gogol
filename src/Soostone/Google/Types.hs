module Soostone.Google.Types
  ( -- * Storage
    GCSFile (..),
    gcsFile_bucket,
    gcsFile_key,
    GCSPath (..),
    gcsPath,
    gcsPathKey,
    GCSPrefix (..),
    gcsPrefix,
    GCSBucketName,
    gcsBucketNameText,
    mkGCSBucketNameTH,
    GCSKey (..),
    gcsKey,
    gcsPrefixTrailingSlash,
    gcsPrefixNoTrailingSlash,
    prefixedFileToGCSKey,

    -- * BigQuery
    BQProjectId,
    bqProjectIdText,
    mkBQProjectIdTH,
    BQDatasetId,
    bqDatasetIdText,
    mkBQDatasetIdTH,
    BQTableId,
    bqTableIdText,
    mkBQTableIdTH,
    LoadBQTableOptions (..),
    loadBQTableOptions_maxBadRecords,
    lenientLoadBQTableOptions,
    strictLoadBQTableOptions,
    BQStandardSQLQuery (..),

    -- * Time
    Seconds (..),
  )
where

-------------------------------------------------------------------------------

import Control.Lens.Extended
import qualified Data.Aeson.Extended as A
import qualified Data.Aeson.Safe as SJ
import qualified Data.String.Conv as SC
import qualified Data.Text as T
import Language.Haskell.TH (Exp, Q)
import Language.Haskell.TH.Syntax (Lift)
import Soostone.Google.Prelude

-------------------------------------------------------------------------------

-- | A file in Google Cloud Storage
data GCSFile = GCSFile
  { _gcsFile_bucket :: GCSBucketName,
    _gcsFile_key :: GCSPath
  }
  deriving stock (Show, Eq)

instance SJ.SafeJSON GCSFile where
  version = 0
  kind = SJ.base
  safeTo f =
    SJ.contain $
      SJ.object
        -- We use the native types here (i.e. not SJ..=$) because they're basic, text and ~string
        [ "bucket" SJ..= _gcsFile_bucket f,
          "key" SJ..= _gcsFile_key f
        ]
  safeFrom = SJ.containWithObject "GCSFile" $ \o -> do
    bucket <- o SJ..: "bucket"
    key <- o SJ..: "key"
    pure $
      GCSFile
        { _gcsFile_bucket = bucket,
          _gcsFile_key = key
        }

-- | Sometimes it's convenient to treat GCS keys as filesystem paths
newtype GCSPath = GCSPath
  { _gcsPath :: Path Rel File
  }
  deriving stock (Show, Eq)

instance A.ToJSON GCSPath where
  toJSON = A.toJSON . toFilePath . _gcsPath

instance A.FromJSON GCSPath where
  parseJSON v = do
    s <- A.parseJSON v
    case parseRelFile s of
      Just f -> pure (GCSPath f)
      Nothing -> fail ("Invalid GCSPath " <> s)

-- | Gogol uses plain Text for keys
gcsPathKey :: Prism' GCSKey GCSPath
gcsPathKey = prism' toKey fromKey
  where
    toKey = GCSKey . SC.toSL . toFilePath . _gcsPath
    fromKey = fmap GCSPath . parseRelFile . SC.toSL . _gcsKey

-- | A storage prefix modeled as a relative directory
newtype GCSPrefix = GCSPrefix
  { _gcsPrefix :: Path Rel Dir
  }
  deriving stock (Show, Eq)
  deriving newtype (A.FromJSON, A.ToJSON)

-- | Append 2 prefixes left above, right below
instance Semigroup GCSPrefix where
  (GCSPrefix a) <> (GCSPrefix b) = GCSPrefix (a </> b)

-- | Convert a storage key prefix into a Text with a guaranteed trailing slash
gcsPrefixTrailingSlash :: GCSPrefix -> Text
gcsPrefixTrailingSlash = T.pack . toFilePath . _gcsPrefix

-- | Convert a storage key prefix into a Text with a guarantee of no trailing slash
gcsPrefixNoTrailingSlash :: GCSPrefix -> Text
gcsPrefixNoTrailingSlash = T.init . gcsPrefixTrailingSlash

prefixedFileToGCSKey :: GCSPrefix -> Path Rel File -> GCSKey
prefixedFileToGCSKey pfx file =
  GCSKey $
    T.pack $
      toFilePath $
        _gcsPrefix pfx </> file

newtype GCSBucketName = GCSBucketName
  { gcsBucketName :: Text
  }
  deriving stock (Show, Eq, Lift)
  deriving newtype (A.ToJSON, A.FromJSON, IsString)

-- | Rough approximation of the bigquery bucket name rules from
-- <https://cloud.google.com/storage/docs/naming-buckets>. [a-z0-9][a-z0-9\-_\.]{2,62}. Avoid
-- using dots because they have special rules and require
-- verification.
gcsBucketNameText :: Prism' Text GCSBucketName
gcsBucketNameText = prism' gcsBucketName $ \txt -> do
  guard (inRange (3, 63) (T.length txt))
  (h, t) <- T.uncons txt
  guard (validFirstChar h)
  guard (T.all validTailChar t)
  pure (GCSBucketName txt)
  where
    validFirstChar = inRange ('a', 'z') <||> inRange ('0', '9')
    validTailChar = inRange ('a', 'z') <||> inRange ('0', '9') <||> (`elem` ['-', '_', '.'])

mkGCSBucketNameTH :: Text -> Q Exp
mkGCSBucketNameTH text =
  previewTH
    ("Expected a valid Google Storage Bucket Name ([a-z0-9][a-z0-9\\-_\\.]{2,62}), received: " <> T.unpack text)
    gcsBucketNameText
    text

newtype GCSKey = GCSKey
  { _gcsKey :: Text
  }
  deriving stock (Show, Eq)
  deriving newtype (A.ToJSON, A.FromJSON)

-------------------------------------------------------------------------------

newtype BQProjectId = BQProjectId
  { bqProjectId :: Text
  }
  deriving stock (Show, Eq, Lift)
  deriving newtype (IsString)

-- | Accepts non-empty bigquery projects. Less strenuous than the
-- actual rules on names because we won't be creating the projects in
-- this app.
bqProjectIdText :: Prism' Text BQProjectId
bqProjectIdText = prism' bqProjectId $ \txt ->
  if not (T.null txt)
    then Just (BQProjectId txt)
    else Nothing

-- | Make a project id that you statically know is valid:
-- @$(mkBQProjectIdTH "valid-project-id")@
mkBQProjectIdTH :: Text -> Q Exp
mkBQProjectIdTH text =
  previewTH
    ("Expected a valid BQProjectID (non-empty), received: " <> T.unpack text)
    bqProjectIdText
    text

instance A.ToJSON BQProjectId where
  toJSON = A.toJSONPrism bqProjectIdText

instance A.FromJSON BQProjectId where
  parseJSON = A.parseJSONPrism bqProjectIdText

newtype BQDatasetId = BQDatasetId
  { bqDatasetId :: Text
  }
  deriving stock (Show, Eq, Lift)
  deriving newtype (IsString)

-- | Accepts non-empty bigquery datasets. Less strenuous than the
-- actual rules on names because we won't be creating the datasets in
-- this app.
bqDatasetIdText :: Prism' Text BQDatasetId
bqDatasetIdText = prism' bqDatasetId $ \txt ->
  if not (T.null txt)
    then Just (BQDatasetId txt)
    else Nothing

-- | Make a dataset id that you statically know is valid:
-- @$(mkBQDatasetIdTH "valid-dataset-id")@
mkBQDatasetIdTH :: Text -> Q Exp
mkBQDatasetIdTH text =
  previewTH
    ("Expected a valid BQDatasetID (non-empty), received: " <> T.unpack text)
    bqDatasetIdText
    text

instance A.FromJSON BQDatasetId where
  parseJSON = A.parseJSONPrism bqDatasetIdText

instance A.ToJSON BQDatasetId where
  toJSON = A.toJSONPrism bqDatasetIdText

newtype BQTableId = BQTableId
  { bqTableId :: Text
  }
  deriving stock (Show, Eq, Lift)
  deriving newtype (IsString)

bqTableIdText :: Prism' Text BQTableId
bqTableIdText = prism' bqTableId $ \txt -> do
  guard (not (T.null txt))
  guard (T.length txt <= 1024)
  guard (T.all (isAlphaNumASCII <||> (== '_')) txt)
  pure (BQTableId txt)

-- | Make a table id that you statically know is valid:
-- @$(mkBQTableIdTH "valid-table-id")@
mkBQTableIdTH :: Text -> Q Exp
mkBQTableIdTH text =
  previewTH
    ("Expected a valid BQTableID (non-empty), received: " <> T.unpack text)
    bqTableIdText
    text

instance SC.StringConv BQTableId Text where
  strConv _leniency = review bqTableIdText

instance A.ToJSON BQTableId where
  toJSON = A.toJSONPrism bqTableIdText

instance A.FromJSON BQTableId where
  parseJSON = A.parseJSONPrism bqTableIdText

-------------------------------------------------------------------------------
data LoadBQTableOptions = LoadBQTableOptions
  { -- | How many bad records to allow when loading a table. It is
    -- usually recommended that you go with "strict", allowing 0 bad
    -- records.
    _loadBQTableOptions_maxBadRecords :: Natural
  }
  deriving stock (Show, Eq)

instance SJ.SafeJSON LoadBQTableOptions where
  version = 0
  kind = SJ.base
  safeFrom = SJ.containWithObject "LoadBQTableOptions" $ \o -> do
    maxBadRecords <- o SJ..: "maxBadRecords"
    pure $
      LoadBQTableOptions
        { _loadBQTableOptions_maxBadRecords = maxBadRecords
        }
  safeTo o =
    SJ.contain $
      SJ.object
        [ "maxBadRecords" SJ..= _loadBQTableOptions_maxBadRecords o
        ]

-- | Lenient options which allow for up to 100K bad records.
lenientLoadBQTableOptions :: LoadBQTableOptions
lenientLoadBQTableOptions =
  LoadBQTableOptions
    { _loadBQTableOptions_maxBadRecords = 100_000
    }

-- | Does not allow any bad records. This is the recommended option.
strictLoadBQTableOptions :: LoadBQTableOptions
strictLoadBQTableOptions =
  LoadBQTableOptions
    { _loadBQTableOptions_maxBadRecords = 0
    }

-------------------------------------------------------------------------------
newtype BQStandardSQLQuery = BQStandardSQLQuery
  { bqStandardSQLQuery :: Text
  }
  deriving stock (Show, Eq)
  deriving newtype (IsString)

-------------------------------------------------------------------------------

makeLenses ''GCSFile

makeLenses ''GCSPath

makeLenses ''GCSPrefix

makeLenses ''GCSBucketName

makeLenses ''GCSKey

makeLenses ''LoadBQTableOptions
