module Soostone.Google.Common
  ( -- * Error handling
    geExceptionStatus,
    is404,

    -- * Running commands
    runGoogle,
    runGoogle',
    recoverGoogle,
    paginate,
  )
where

-------------------------------------------------------------------------------

import qualified Control.Retry as Retry
import qualified Data.Conduit as C
import qualified Gogol as Google
import qualified Network.HTTP.Client as HC
import qualified Network.HTTP.Types.Status as NHTS
import Soostone.Google.Prelude

-------------------------------------------------------------------------------

is404 :: Google.Error -> Bool
is404 e
  | (NHTS.statusCode <$> geExceptionStatus e) == Just 404 = True
  | otherwise = False

-------------------------------------------------------------------------------
geExceptionStatus :: Google.Error -> Maybe NHTS.Status
geExceptionStatus (Google.TransportError he) = httpExceptionStatus he
geExceptionStatus (Google.ServiceError se) = Just (Google._serviceStatus se)
geExceptionStatus (Google.SerializeError _) = Nothing

-------------------------------------------------------------------------------
httpExceptionStatus :: HC.HttpException -> Maybe NHTS.Status
httpExceptionStatus (HC.HttpExceptionRequest _ (HC.StatusCodeException r _)) = Just (HC.responseStatus r)
httpExceptionStatus _ = Nothing

-------------------------------------------------------------------------------

-- | Recovers from transient errors (status 4XX) with a reasonable
-- retry policy.
recoverGoogle ::
  forall m a.
  ( MonadMask m,
    KatipContext m
  ) =>
  Retry.RetryPolicyM m ->
  m a ->
  m a
recoverGoogle retryPolicy m = do
  Retry.recovering retryPolicy handlers (const m)
  where
    handlers =
      const
        <$> [ handler geExceptionStatus,
              -- Error includes below but just to be safe we'll catch these exceptions directly too
              handler httpExceptionStatus
            ]
    handler :: (Exception e) => (e -> Maybe NHTS.Status) -> Handler m Bool
    handler getStatus = Handler $ \se ->
      case fromException se of
        Just e -> do
          -- If for some reason its missing a status, assume it wasn't a client error and retry
          let isClientError = maybe False NHTS.statusIsClientError (getStatus e)
          if isClientError
            then do
              $(logTM) ErrorS ("Error was a client error. Cannot retry. " <> showLS e)
              pure False
            else do
              $(logTM) ErrorS ("Error was a not a client error. Retrying. " <> showLS e)
              pure True
        Nothing -> pure False

-------------------------------------------------------------------------------

-- | Runs a google operation in a given environment, applying the
-- standard retry policy. This also is designed to cast your env with
-- whatever scopes it has to the scopes needed for the request, thus
-- "defeating" the type-level scope accounting that gogol does. We've
-- found over time that the scope system in gogol is hard to use and
-- doesn't provide much value in return. I suspect the author might
-- agree but there isn't really any active effort being put into
-- working on the library so the scope system is here to stay for now.
runGoogle ::
  ( MonadMask m,
    KatipContext m,
    MonadUnliftIO m
  ) =>
  Retry.RetryPolicyM m ->
  ResourceT m a ->
  m a
runGoogle retryPolicy f = Google.runResourceT $ runGoogle' retryPolicy f

-------------------------------------------------------------------------------

-- | Like 'runGoogle' but happens inside ResourceT so you can combine
-- multiple uses of this and get the retry effects while also sharing
-- resources more granularly.
runGoogle' ::
  forall m a.
  ( MonadMask m,
    KatipContext m
  ) =>
  Retry.RetryPolicyM m ->
  ResourceT m a ->
  ResourceT m a
runGoogle' retryPolicy = recoverGoogle (Retry.natTransformRetryPolicy lift retryPolicy)

-------------------------------------------------------------------------------

-- | Repeatedly issue a request, substituting the next page token
-- until the pages run out
paginate ::
  ( Monad m
  ) =>
  -- | How do we get the next page token from the response?
  (Google.Rs req -> Maybe Text) ->
  -- | How do we inject a page token into the request
  (Maybe Text -> req -> req) ->
  -- | how do we run the request?
  (req -> m (Google.Rs req)) ->
  req ->
  C.ConduitM () (Google.Rs req) m ()
paginate getPageToken setPageToken sendReq req = do
  resp <- lift (sendReq req)
  C.yield resp
  case getPageToken resp of
    Nothing -> pure ()
    Just nextToken -> paginate getPageToken setPageToken sendReq (setPageToken (Just nextToken) req)
