module Soostone.Google.Storage
  ( -- * API Calls
    deleteObject,
    uploadLocalFile,
    objectExists,
    streamObjectsInPrefix,
    downloadObjectToFile,

    -- * Conversion
    humanReadableGCSURL,
    humanReadableGCSURL',
    urlEncodedGCSKey,
  )
where

-------------------------------------------------------------------------------

import Control.Lens.Extended
import Control.Monad.Trans.Resource (MonadResource)
import qualified Control.Retry as Retry
import qualified Data.ByteString.Builder as BB
import qualified Data.Conduit as C
import qualified Data.Conduit.Combinators as CC
import qualified Data.String.Conv as SC
import qualified Gogol as Google
import qualified Gogol.Storage as GCS
import qualified Soostone.Google.Common as Common
import Soostone.Google.Prelude
import Soostone.Google.Types
import qualified URI.ByteString as URI

-------------------------------------------------------------------------------

-- | Deletes a key. Is a no-op if the key does not exist
deleteObject ::
  ( MonadUnliftIO m,
    KatipContext m,
    MonadMask m,
    Google.AllowRequest GCS.StorageObjectsDelete scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  GCSBucketName ->
  GCSKey ->
  ResourceT m ()
deleteObject retryPolicy env bucketName key = do
  $(logTM) DebugS ("Deleting " <> ls (humanReadableGCSURL' bucketName key))
  Common.runGoogle (Retry.natTransformRetryPolicy lift retryPolicy) $
    catchIf
      Common.is404
      (Google.send env cmd)
      (\_ -> return ())
  where
    cmd =
      GCS.newStorageObjectsDelete
        (review gcsBucketNameText bucketName)
        -- DELETE needs key encoding
        (urlEncodedGCSKey key)

-------------------------------------------------------------------------------
uploadLocalFile ::
  ( KatipContext m,
    MonadUnliftIO m,
    MonadMask m,
    Google.AllowRequest GCS.StorageObjectsInsert scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  GCSBucketName ->
  GCSKey ->
  Path a File ->
  ResourceT m GCS.Object
uploadLocalFile retryPolicy googleEnv bucketName key file = do
  body <- Google.sourceBody fileStr
  $(logTM)
    DebugS
    ("Uploading " <> ls fileStr <> " to " <> ls (humanReadableGCSURL' bucketName key))
  Common.runGoogle (Retry.natTransformRetryPolicy lift retryPolicy) $
    Google.upload googleEnv cmd body
  where
    fileStr = toFilePath file
    cmd =
      GCS.newStorageObjectsInsert (review gcsBucketNameText bucketName) GCS.newObject
        & #name ?~ _gcsKey key

-------------------------------------------------------------------------------
objectExists ::
  ( MonadMask m,
    KatipContext m,
    MonadUnliftIO m,
    Google.AllowRequest GCS.StorageObjectsGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  GCSBucketName ->
  GCSKey ->
  m Bool
objectExists retryPolicy env bucketName gk =
  Common.runGoogle retryPolicy $
    catchIf
      Common.is404
      (True <$ Google.send env cmd)
      (\_ -> return False)
  where
    cmd =
      GCS.newStorageObjectsGet
        (review gcsBucketNameText bucketName)
        -- GET needs key encoding
        (urlEncodedGCSKey gk)

-------------------------------------------------------------------------------

-- | Stream objects non-recursively
streamObjectsInPrefix ::
  ( MonadUnliftIO m,
    KatipContext m,
    MonadMask m,
    Google.AllowRequest GCS.StorageObjectsList scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  GCSBucketName ->
  GCSPrefix ->
  C.ConduitM () GCS.Object m ()
streamObjectsInPrefix retryPolicy env bucket prefix = do
  Common.paginate
    (view #nextPageToken)
    (set #pageToken)
    (Common.runGoogle retryPolicy . Google.send env)
    req
    C..| CC.concatMap (fromMaybe [] . view #items)
  where
    req =
      GCS.newStorageObjectsList (review gcsBucketNameText bucket)
        & #prefix ?~ gcsPrefixTrailingSlash prefix

------------------------------------------------------------------------------

-- | Stream an object over HTTPS. Unless you can guarantee that you
-- can consume this immediately, you should stream this to a temporary
-- file to avoid networking timesouts and such.
streamObject ::
  ( MonadResource m,
    Google.AllowRequest GCS.StorageObjectsGet scopes
  ) =>
  Google.Env scopes ->
  GCSBucketName ->
  GCSKey ->
  m (Google.Rs (Google.MediaDownload GCS.StorageObjectsGet))
streamObject env bucketName key =
  Google.download env $
    GCS.newStorageObjectsGet
      (review gcsBucketNameText bucketName)
      -- object GET require you to urlencode the key for some reason where other APIs don't
      (urlEncodedGCSKey key)

downloadObjectToFile ::
  ( MonadUnliftIO m,
    KatipContext m,
    MonadMask m,
    Google.AllowRequest GCS.StorageObjectsGet scopes
  ) =>
  Retry.RetryPolicyM m ->
  Google.Env scopes ->
  GCSBucketName ->
  GCSKey ->
  Path b File ->
  m ()
downloadObjectToFile retryPolicy env bucketName key dest = do
  Common.runGoogle retryPolicy $ do
    stream <- streamObject env bucketName key
    liftIO . runResourceT . C.runConduit $
      stream C..| CC.sinkFile (toFilePath dest)

-------------------------------------------------------------------------------
urlEncodedGCSKey :: GCSKey -> Text
urlEncodedGCSKey = SC.toSL . BB.toLazyByteString . URI.urlEncodePath . SC.toSL . _gcsKey

-------------------------------------------------------------------------------

-- | Converts a bucket and key into a gs:// url.
humanReadableGCSURL :: GCSFile -> Text
humanReadableGCSURL f =
  humanReadableGCSURL'
    (_gcsFile_bucket f)
    (review gcsPathKey (_gcsFile_key f))

humanReadableGCSURL' :: GCSBucketName -> GCSKey -> Text
humanReadableGCSURL' bucket (GCSKey key) =
  "gs://" <> review gcsBucketNameText bucket <> "/" <> key
