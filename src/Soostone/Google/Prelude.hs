module Soostone.Google.Prelude
  ( module Reexport,
    Fail.MonadFail (..),

    -- * Boolean logic
    (<&&>),
    (<||>),

    -- * Text
    isAlphaNumASCII,
    isAlphaASCII,

    -- * Time
    Seconds (..),
    secondsToMicros,
    Microseconds (..),
    timeout,
    threadDelay,
    Milliseconds (..),
    millisecondsToNDT,
    posixMilliseconds,
  )
where

-------------------------------------------------------------------------------

import Control.Applicative as Reexport
import qualified Control.Concurrent as Conc
import Control.Error as Reexport (ExceptT (..), fmapLT, hoistMaybe, hush, note, noteT, readMay, runExceptT, throwE)
import Control.Exception.Safe.Extended as Reexport
import Control.Monad as Reexport
import qualified Control.Monad.Fail as Fail
import Control.Monad.IO.Class as Reexport
import Control.Monad.IO.Unlift as Reexport
import Control.Monad.Primitive as Reexport (PrimMonad)
import Control.Monad.Trans.Class as Reexport
import Control.Monad.Trans.Resource as Reexport (ReleaseKey, ResourceT, register, release, runResourceT)
import qualified Data.Aeson.Extended as A
import Data.ByteString as Reexport (ByteString)
import Data.Coerce as Reexport (coerce)
import Data.Foldable as Reexport
import Data.Generics.Labels ()
import Data.Int as Reexport
import Data.Ix as Reexport (inRange)
import Data.List.NonEmpty as Reexport (NonEmpty (..), nonEmpty)
import Data.Maybe as Reexport
import Data.String as Reexport
import Data.Sum.Errors as Reexport
import Data.Tagged as Reexport (Tagged (..), untag)
import Data.Text as Reexport (Text)
import Data.Time as Reexport (Day, NominalDiffTime, UTCTime)
import qualified Data.Time.Clock.POSIX as POSIX
import Data.Universe.Class as Reexport
import GHC.Generics as Reexport (Generic)
import Instances.TH.Lift ()
import Katip as Reexport
import Numeric.Natural as Reexport
import Path.Extended as Reexport
import qualified UnliftIO.Timeout as Timeout
import Prelude as Reexport

-------------------------------------------------------------------------------

-- | Can be used to combine predicate functions, e.g. isDigit <&&> (/= '3')
(<&&>) :: Applicative f => f Bool -> f Bool -> f Bool
(<&&>) = liftA2 (&&)

-- | Can be used to combine predicate functions, e.g. isDigit <||> isAlpha
(<||>) :: Applicative f => f Bool -> f Bool -> f Bool
(<||>) = liftA2 (||)

-------------------------------------------------------------------------------
isAlphaNumASCII :: Char -> Bool
isAlphaNumASCII =
  inRange ('0', '9')
    <||> isAlphaASCII

-------------------------------------------------------------------------------
isAlphaASCII :: Char -> Bool
isAlphaASCII =
  inRange ('a', 'z')
    <||> inRange ('A', 'Z')

-------------------------------------------------------------------------------
newtype Seconds = Seconds
  { seconds :: Natural
  }
  deriving stock (Show, Eq, Ord)
  deriving newtype (A.FromJSON, A.ToJSON)

secondsToMicros :: Seconds -> Microseconds
secondsToMicros = Microseconds . (* 1_000_000) . seconds

newtype Microseconds = Microseconds
  { microseconds :: Natural
  }
  deriving stock (Show, Eq, Ord)
  deriving newtype (A.FromJSON, A.ToJSON)

newtype Milliseconds = Milliseconds
  { milliseconds :: Natural
  }
  deriving stock (Show, Eq, Ord)

posixMilliseconds :: Milliseconds -> POSIX.POSIXTime
posixMilliseconds = millisecondsToNDT

millisecondsToNDT :: Milliseconds -> NominalDiffTime
millisecondsToNDT = (/ 1000) . fromIntegral . milliseconds

-------------------------------------------------------------------------------

-- | @timeout@ with a better-typed time parameter
timeout ::
  ( MonadUnliftIO m
  ) =>
  Microseconds ->
  m a ->
  m (Maybe a)
timeout = Timeout.timeout . fromIntegral . microseconds

-------------------------------------------------------------------------------

-- | @threadDelay@ with a better-typed time parameter
threadDelay :: Microseconds -> IO ()
threadDelay = Conc.threadDelay . fromIntegral . microseconds
