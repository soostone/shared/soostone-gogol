module Data.UUID.Extended
  ( module Data.UUID,
    uuidText,
  )
where

-------------------------------------------------------------------------------

import Control.Lens
import Data.Text (Text)
import Data.UUID

-------------------------------------------------------------------------------

uuidText :: Prism' Text UUID
uuidText = prism' toText fromText
