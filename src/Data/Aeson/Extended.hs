module Data.Aeson.Extended
  ( module A,

    -- * Conversion
    toJSONPrism,
    parseJSONPrism,

    -- * Streams
    parseJSONStream,

    -- * Re-exports
    toHashMapText,
  )
where

-------------------------------------------------------------------------------

import Control.Lens
import Data.Aeson as A
import Data.Aeson.Parser as A
#if MIN_VERSION_aeson (2, 0, 0)
import qualified Data.Aeson.KeyMap as A
import qualified Data.HashMap.Strict as HM
import qualified Data.Text as T
#endif
import Data.Aeson.Types as AT
import Data.ByteString (ByteString)
import qualified Data.Conduit as C
import qualified Data.Conduit.Attoparsec as CA
import qualified Data.Conduit.Combinators as CC
import Data.Typeable
import Prelude

-------------------------------------------------------------------------------

-- | Default implementation of ToJSON if you have a prism to a type
-- that has a JSON instance
toJSONPrism ::
  (A.ToJSON b) =>
  Prism' b a ->
  (a -> A.Value)
toJSONPrism pris = A.toJSON . review pris

parseJSONPrism ::
  forall a b.
  ( A.FromJSON b,
    Show b,
    Typeable a
  ) =>
  Prism' b a ->
  (A.Value -> AT.Parser a)
parseJSONPrism pris v = do
  b <- A.parseJSON v
  case preview pris b of
    Just a -> pure a
    Nothing -> fail ("Could not parse " <> show (typeRep (Proxy :: Proxy a)) <> " from " <> show b)

-- | Take a stream of bytestrings and turn it into a stream of JSON
-- values (or errors). The JSON can either have no delimiter *or* be
-- delimited by newlines. NOTE: this is meant for trusted sources
-- since it takes unbounded line lengths and thus could use a lot of
-- memory if a message was so crafted.
parseJSONStream ::
  (Monad m) =>
  C.ConduitM ByteString (Either CA.ParseError A.Value) m ()
parseJSONStream =
  CC.linesUnboundedAscii
    C..| CA.conduitParserEither A.json'
    C..| CC.map (fmap snd)

#if MIN_VERSION_aeson (2, 0, 0)
toHashMapText :: A.KeyMap A.Value -> HM.HashMap T.Text A.Value
toHashMapText = A.toHashMapText
#else
toHashMapText :: A.Object -> HM.HashMap T.Text A.Value
toHashMapText = id
#endif
