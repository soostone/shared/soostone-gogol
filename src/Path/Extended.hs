module Path.Extended
  ( module Path,
    relDirPath,
  )
where

-------------------------------------------------------------------------------

import Control.Lens
import Path
import Prelude

-------------------------------------------------------------------------------

relDirPath :: Prism' FilePath (Path Rel Dir)
relDirPath = prism' toFilePath parseRelDir
