{ withHoogle ? false, withCommitHook ? true }:
with (import ./. { });

let
  python-commit-hooks =
    pkgs.python3Packages.pre-commit-hooks.overridePythonAttrs
    (_: { doCheck = false; });
  pre-commit-hooks = import sources.pre-commit-hooks;
  pre-commit-config = if withCommitHook then {
    src = ./.;
    tools = pkgs;
    hooks = {
      ormolu = {
        enable = true;
        settings = {
          defaultExtensions =
            [ "TypeApplications" "BangPatterns" "TemplateHaskell" "CPP" ];
        };
      };
      hlint.enable = true;
      yamllint.enable = true;
      nixfmt = {
        enable = true;
        excludes = [ "nix/default.nix" ];
      };
      trailing-whitespace = {
        enable = true;
        name = "trailing-whitespace";
        entry = "${python-commit-hooks}/bin/trailing-whitespace-fixer";
        types = [ "text" ];
      };
      end-of-file = {
        enable = true;
        name = "end-of-file";
        entry = "${python-commit-hooks}/bin/end-of-file-fixer";
        types = [ "text" ];
      };
    };
  } else {
    src = ./.;
  };
  pre-commit-check = pre-commit-hooks.run pre-commit-config;

in haskellPkgs.shellFor {
  packages = p: with p; [ p.soostone-gogol ];
  inherit withHoogle;

  nativeBuildInputs = with pkgs; [
    cabal-install
    ghcid
    haskell-language-server
    hlint
    hpack
    niv
    ormolu
  ];

  shellHook = ''
    { ${pre-commit-check.shellHook} } 2> /dev/null
  '';
}
