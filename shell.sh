#!/usr/bin/env bash
# Provision build dependencies with nix and drop into a nix shell. We
# specifically do not recurse here to avoid having to pull
# dependencies that well doesn't have access to.
git submodule sync
git submodule update --init
nix-shell "$@"
