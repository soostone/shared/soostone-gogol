module Soostone.Tests.Helpers where

-------------------------------------------------------------------------------

import Control.Lens hiding (universe)
import Control.Monad.IO.Class
import qualified Data.Aeson as A
import qualified Data.UUID as UUID
#if MIN_VERSION_aeson (2, 0, 0)
import qualified Data.Aeson.KeyMap as A
#endif
import qualified Data.Aeson.Safe as SJ
import Data.ByteString (ByteString)
import Data.Foldable
import qualified Data.HashMap.Strict as HMS
import Data.Hashable
import Data.Int
import Data.List.NonEmpty (NonEmpty (..))
import Data.Ratio
import qualified Data.SafeJSON.Test as SJ
import Data.Scientific
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time
import Data.Universe.Class
import qualified Data.Vector as V
import qualified GHC.Generics as G
import qualified Gogol.BigQuery as BQ
import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Numeric.Natural
import Path
import Soostone.Google.BigQuery
import Soostone.Google.BigQuery.Schema as BQS
import Soostone.Google.Types
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Hedgehog
import Prelude

-------------------------------------------------------------------------------

genUniverse :: Universe a => Gen a
genUniverse = Gen.element universe

trippingJSON ::
  ( A.ToJSON a,
    A.FromJSON a,
    Show a,
    Eq a,
    MonadTest m
  ) =>
  a ->
  m ()
trippingJSON a = tripping a A.encode A.eitherDecode

trippingPrism ::
  ( Show a,
    Eq a,
    MonadTest m,
    Show b
  ) =>
  Prism' b a ->
  a ->
  m ()
trippingPrism pris a = tripping a (review pris) (preview pris)

trippingIso ::
  ( Show a,
    Eq a,
    MonadTest m,
    Show b,
    Eq b
  ) =>
  Iso' a b ->
  a ->
  b ->
  m ()
trippingIso i a b = do
  tripping a (view i) (Identity . view (from i))
  tripping b (view (from i)) (Identity . view i)

safeJSONTests ::
  forall a.
  ( SJ.SafeJSON a,
    Show a,
    Eq a
  ) =>
  Gen a ->
  [TestTree]
safeJSONTests genA =
  [ testProperty "cycles via SafeJSON" $
      property $ do
        a <- forAll genA
        liftIO (SJ.testRoundTrip a),
    testCase "SafeJSON is consistent" (SJ.testConsistency @a)
  ]

genGCSFile :: Gen GCSFile
genGCSFile = do
  bucket <- genGCSBucketName
  key <- genGCSPath
  pure $
    GCSFile
      { _gcsFile_bucket = bucket,
        _gcsFile_key = key
      }

genLoadBQTableOptions :: Gen LoadBQTableOptions
genLoadBQTableOptions = do
  maxBadRecords <- genNatural
  pure $
    LoadBQTableOptions
      { _loadBQTableOptions_maxBadRecords = maxBadRecords
      }

genGCSBucketName :: Gen GCSBucketName
genGCSBucketName = do
  headChar <- Gen.element validHeadChars
  tailChars <- Gen.list (Range.linear 2 62) (Gen.element validTailChars)
  let str = headChar : tailChars
  case preview gcsBucketNameText (T.pack str) of
    Just a -> pure a
    Nothing -> fail ("genGCSBucketName bug: " <> str)
  where
    validHeadChars = ['a' .. 'z'] <> ['0' .. '9']
    validTailChars = ['a' .. 'z'] <> ['0' .. '9'] <> ['-', '_', '.']

genGCSPath :: Gen GCSPath
genGCSPath = GCSPath <$> genRelFile

genNatural :: Gen Natural
genNatural = fromIntegral <$> Gen.int (Range.linear 0 maxBound)

genRelFile :: Gen (Path Rel File)
genRelFile = Gen.just $ do
  parentMay <- Gen.maybe genRelDir
  fnRaw <- Gen.string (Range.linear 1 10) Gen.alphaNum
  pure $ case parseRelFile fnRaw of
    Nothing -> Nothing
    Just fn -> Just (maybe fn (</> fn) parentMay)

genRelDir :: Gen (Path Rel Dir)
genRelDir = Gen.just $ do
  segs <- Gen.nonEmpty (Range.linear 1 10) (Gen.string (Range.linear 1 10) Gen.alphaNum)
  pure $ case sequence (parseRelDir <$> segs) of
    Nothing -> Nothing
    Just (segsHead :| segsTail) -> Just (foldl' (</>) segsHead segsTail)

genBQProjectId :: Gen BQProjectId
genBQProjectId = do
  maybe (fail "genBQProjectId bug") pure . preview bqProjectIdText =<< genTextToken1

genBQDatasetId :: Gen BQDatasetId
genBQDatasetId = do
  maybe (fail "genBQDatasetId bug") pure . preview bqDatasetIdText =<< genTextToken1

genBQTableId :: Gen BQTableId
genBQTableId =
  maybe (fail "genBQTableId bug") pure . preview bqTableIdText =<< Gen.text (Range.linear 1 1024) (Gen.element chars)
  where
    chars =
      ['a' .. 'z']
        <> ['A' .. 'Z']
        <> ['0' .. '9']
        <> ['_']

genTextToken1 :: Gen Text
genTextToken1 = Gen.text (Range.linear 1 20) Gen.unicode

genTextToken :: Gen Text
genTextToken = Gen.text (Range.linear 0 20) Gen.unicode

genGCSPrefix :: Gen GCSPrefix
genGCSPrefix = GCSPrefix <$> genRelDir

genCreateTableOptions_V0 :: Gen CreateTableOptions_V0
genCreateTableOptions_V0 = do
  timePartitioning <- Gen.maybe genBQTimePartitioning
  clustering <- Gen.maybe (Gen.nonEmpty (Range.linear 1 20) genBQFieldName)
  pure $
    CreateTableOptions_V0
      { _createTableOptions_V0_timePartitioning = timePartitioning,
        _createTableOptions_V0_clustering = clustering
      }

genCreateTableOptions :: Gen CreateTableOptions
genCreateTableOptions = do
  tablePartitioning <- genTablePartitioning
  clustering <- Gen.maybe (Gen.nonEmpty (Range.linear 1 20) genBQFieldName)
  expirationTime <- Gen.maybe genUTCTime
  pure $
    CreateTableOptions
      { _createTableOptions_partitioning = tablePartitioning,
        _createTableOptions_clustering = clustering,
        _createTableOptions_expirationTime = expirationTime
      }

genBQTimePartitioning :: Gen BQ.TimePartitioning
genBQTimePartitioning = do
  fieldName <- fmap (review BQS.fieldNameText) <$> Gen.maybe genBQFieldName
  expirationMs <- Gen.maybe (Gen.int64 Range.linearBounded)
  requirePartitionFilter <- Gen.maybe Gen.bool
  ty <- Gen.maybe (pure "DAY")
  pure $
    BQ.newTimePartitioning
      & #field .~ fieldName
      & #expirationMs .~ expirationMs
      & #requirePartitionFilter .~ requirePartitionFilter
      & #type' .~ ty

genBQRangePartitioning :: Gen BQ.RangePartitioning
genBQRangePartitioning = do
  fieldName <- fmap (review BQS.fieldNameText) <$> Gen.maybe genBQFieldName
  let pRange = Nothing
  pure $
    BQ.newRangePartitioning
      & #field .~ fieldName
      & #range .~ pRange

genTablePartitioning :: Gen TablePartitioning
genTablePartitioning = do
  Gen.choice
    [ TablePartitioning_Time <$> genBQTimePartitioning,
      TablePartitioning_Range <$> genBQRangePartitioning,
      pure TablePartitioning_None
    ]

genBQFieldName :: Gen BQS.FieldName
genBQFieldName = do
  headChar <- Gen.element (['a' .. 'z'] <> ['A' .. 'Z'] <> ['_'])
  tailChars <- Gen.string (Range.linear 0 127) (Gen.element (['a' .. 'z'] <> ['A' .. 'Z'] <> ['0' .. '9'] <> ['_']))
  let txt = T.pack (headChar : tailChars)
  maybe (fail "genBQFieldName bug") pure (preview BQS.fieldNameText txt)

genBQFieldSchema :: Gen BQS.FieldSchema
genBQFieldSchema = do
  mode <- genBQFieldMode
  ty <- genBQFieldType
  pure $
    BQS.FieldSchema
      { _fieldSchema_mode = mode,
        _fieldSchema_type = ty
      }

genBQFieldMode :: Gen BQS.FieldMode
genBQFieldMode = genUniverse

genBQFieldType :: Gen BQS.FieldType
genBQFieldType = gen

genBQSchema :: Gen BQS.Schema
genBQSchema =
  BQS.mkSchema
    <$> Gen.nonEmpty (Range.linear 1 20) ((,) <$> genBQFieldName <*> genBQFieldSchema)

genGCSKey :: Gen GCSKey
genGCSKey = GCSKey <$> genTextToken

genBQTimestamp :: Gen BQTimestamp
genBQTimestamp = BQTimestamp <$> genUTCTime

-- | Generate a 'UTCTime' whose day is valid (according to 'genDay') and whose
-- time falls anywhere within 24 hours of that day.
--
-- Note that this does not account for leap seconds, which is to say that the
-- time-of-day may fall within the range (0, 86,400] seconds.
genUTCTime :: Gen UTCTime
genUTCTime = do
  day <- genDay
  let secondsInDayRange = Range.constant 0 ((24 * 60 * 60) - 1)
  time <- fromInteger <$> Gen.integral secondsInDayRange
  pure $ UTCTime day time

genTimeOfDay :: Gen TimeOfDay
genTimeOfDay = do
  hour <- Gen.int (Range.linear 0 23)
  minute <- Gen.int (Range.linear 0 59)
  sec <- fromIntegral <$> Gen.int (Range.linear 0 60) -- allow leap secs
  pure $
    TimeOfDay
      { todHour = hour,
        todMin = minute,
        todSec = sec
      }

-- | Generate an 'Day', whose year is clustered according to 'genYear' and whose
-- day of month will always be valid for the given year and month.
genDay :: Gen Day
genDay = do
  year <- genYear
  month <- genMonth
  day <- genDayOfMonth year month
  pure (fromGregorian year month day)

-- | Generate an arbitrary year between 1970 and 2035, clustering around 2020.
genYear :: Gen Integer
genYear = Gen.integral $ Range.constantFrom 2020 1970 2035

genMonth :: Gen Int
genMonth = Gen.integral $ Range.constant 1 12

-- | Generate an integer corresponding to a valid day the given year and month.
genDayOfMonth ::
  -- | Year
  Integer ->
  -- | Month
  Int ->
  Gen Int
genDayOfMonth year month =
  Gen.integral $
    Range.constant 1 (gregorianMonthLength year month)

genBQSValue :: Gen BQS.Value
genBQSValue = gen

genRational :: Gen Rational
genRational =
  (%)
    <$> genInteger
    <*> Gen.filter (/= 0) genInteger

-- | Generates Integers in the 64-bit range
genInteger :: Gen Integer
genInteger = genInteger' Range.linearBounded

genInteger' :: Range Int64 -> Gen Integer
genInteger' = fmap toInteger . Gen.int64

-- | We reign in the exponential component here because if we let it
-- run wild over the full range of Int, the test cases end up becoming
-- extremely expensive to generate.
genScientific :: Gen Scientific
genScientific = scientific <$> genInteger <*> Gen.int (Range.linear (-100) 100)

genUUID :: Gen UUID.UUID
genUUID = do
  w1 <- Gen.word32 Range.linearBounded
  w2 <- Gen.word32 Range.linearBounded
  w3 <- Gen.word32 Range.linearBounded
  w4 <- Gen.word32 Range.linearBounded
  pure (UUID.fromWords w1 w2 w3 w4)

-- | Generates a valid PartitionNumber in the accepted range (1~4000)
genPartitionNumber :: Gen PartitionNumber
genPartitionNumber = Gen.just (preview partitionNumberInt64 <$> Gen.int64 (Range.linear 1 4000))

-- | A necessary evil for generators that can generically make sure
-- all constructors of your sum type are covered. The tradeoff is that
-- you have no control over the range at the per-test level, so use
-- this sparingly where full coverage is more valuable than
-- fine-tuning generators.
class HasGen a where
  gen :: Gen a
  default gen :: (G.Generic a, GHasGen (G.Rep a)) => Gen a
  gen = Gen.choice (fmap G.to <$> ggen)

class GHasGen f where
  ggen :: [Gen (f a)]

instance GHasGen G.U1 where
  ggen = [pure G.U1]

instance (HasGen a) => GHasGen (G.K1 i a) where
  ggen = [G.K1 <$> gen]

instance (GHasGen a, GHasGen b) => GHasGen (a G.:*: b) where
  ggen = [(G.:*:) <$> f <*> g | f <- ggen, g <- ggen]

instance (GHasGen a, GHasGen b) => GHasGen (a G.:+: b) where
  ggen = (fmap G.L1 <$> ggen) <> (fmap G.R1 <$> ggen)

-- we don't care about metadata, lift over it
instance (GHasGen c) => GHasGen (G.M1 _a _b c) where
  ggen = fmap G.M1 <$> ggen

instance HasGen BQS.FieldType

instance HasGen BQS.FieldTypeScalar

instance HasGen BQS.Value

instance HasGen Rational where gen = genRational

instance HasGen Int64 where
  gen = Gen.int64 Range.linearBounded

instance HasGen Double where gen = Gen.double (Range.linearFrac (-999999) 999999)

instance HasGen Bool where gen = Gen.bool

instance HasGen Text where gen = genTextToken

instance HasGen ByteString where gen = Gen.bytes (Range.linear 0 1024)

instance HasGen Day where gen = genDay

instance HasGen UTCTime where gen = genUTCTime

instance HasGen BQTimestamp where gen = genBQTimestamp

instance HasGen TimeOfDay where gen = genTimeOfDay

instance HasGen A.Value

#if MIN_VERSION_base (4, 16, 0)
instance (HasGen k, HasGen v, Hashable k) => HasGen (HMS.HashMap k v) where
  gen = HMS.fromList <$> Gen.list (Range.linear 0 10) ((,) <$> gen <*> gen)
#else
instance (HasGen k, HasGen v, Eq k, Hashable k) => HasGen (HMS.HashMap k v) where
  gen = HMS.fromList <$> Gen.list (Range.linear 0 10) ((,) <$> gen <*> gen)
#endif

#if MIN_VERSION_aeson (2, 0, 0)
instance HasGen v => HasGen (A.KeyMap v) where
  gen = fmap A.fromHashMapText gen
#endif

instance HasGen a => HasGen (V.Vector a) where
  gen = V.fromList <$> Gen.list (Range.linear 0 10) gen

instance HasGen Scientific where gen = genScientific
