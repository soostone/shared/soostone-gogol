module Soostone.Tests.Google
  ( fastTests,
  )
where

-------------------------------------------------------------------------------

import qualified Data.Text as T
import Hedgehog
import Soostone.Google.Types
import Soostone.Tests.Helpers
import Test.Tasty
import Test.Tasty.Hedgehog
import Prelude

-------------------------------------------------------------------------------

fastTests :: TestTree
fastTests =
  testGroup
    "Soostone.Google"
    [ testGroup "GCSFile" $ safeJSONTests genGCSFile,
      testGroup "LoadBQTableOptions" $ safeJSONTests genLoadBQTableOptions,
      testProperty "bqProjectIdText Prism" $
        property $ do
          trippingPrism bqProjectIdText =<< forAll genBQProjectId,
      testProperty "BQProjectId JSON" $
        property $ do
          trippingJSON =<< forAll genBQProjectId,
      testProperty "bqDatasetIdText Prism" $
        property $ do
          trippingPrism bqDatasetIdText =<< forAll genBQDatasetId,
      testProperty "BQDatasetId JSON" $
        property $ do
          trippingJSON =<< forAll genBQDatasetId,
      testProperty "bqTableIdText Prism" $
        property $ do
          trippingPrism bqTableIdText =<< forAll genBQTableId,
      testProperty "BQTableId JSON" $
        property $ do
          trippingJSON =<< forAll genBQTableId,
      testProperty "GCSPrefix JSON" $
        property $ do
          trippingJSON =<< forAll genGCSPrefix,
      testProperty "GCSPath JSON" $
        property $ do
          trippingJSON =<< forAll genGCSPath,
      testProperty "GCSKey JSON" $
        property $ do
          trippingJSON =<< forAll genGCSKey,
      testProperty "GCSBucketName JSON" $
        property $ do
          trippingJSON =<< forAll genGCSBucketName,
      testProperty "gcsBucketNameText Prism" $
        property $ do
          trippingPrism gcsBucketNameText =<< forAll genGCSBucketName,
      testProperty "gcsPrefixTrailingSlash produces a trailing slash" $
        property $ do
          txt <- gcsPrefixTrailingSlash <$> forAll genGCSPrefix
          T.last txt === '/',
      testProperty "gcsPrefixNoTrailingSlash produces no trailing slash" $
        property $ do
          txt <- gcsPrefixNoTrailingSlash <$> forAll genGCSPrefix
          T.last txt /== '/'
    ]
