module Soostone.Tests.Google.BigQuery.Schema
  ( fastTests,
  )
where

-------------------------------------------------------------------------------

import Control.Lens
import qualified Data.Aeson as A
import Hedgehog
import Soostone.Google.BigQuery.Schema
import Soostone.Tests.Helpers
import Test.Tasty
import Test.Tasty.Hedgehog
import Prelude

-------------------------------------------------------------------------------

fastTests :: TestTree
fastTests =
  testGroup
    "Soostone.Google.BigQuery.Schema"
    [ testGroup "FieldName SafeJSON" (safeJSONTests genBQFieldName),
      testGroup "FieldSchema SafeJSON" (safeJSONTests genBQFieldSchema),
      testGroup "FieldMode SafeJSON" (safeJSONTests genBQFieldMode),
      testGroup "FieldType SafeJSON" (safeJSONTests genBQFieldType),
      testGroup "Schema SafeJSON" (safeJSONTests genBQSchema),
      testProperty "BQTimestamp JSON" $
        property $
          trippingJSON =<< forAll genBQTimestamp,
      testProperty "ToValue/parseValueHint w/ consistent types" . withShrinks 1 $
        property $ do
          -- saw off some unstable serialization cases
          bqVal <-
            forAll $
              genBQSValue <&> \v -> case v of
                Value_Numeric n -> Value_Numeric (fromInteger (truncate n))
                _ -> v
          case valueFieldType bqVal of
            Nothing -> pure ()
            Just typ -> do
              let json = A.toJSON bqVal
              let parsed = parseValueHint typ json
              -- annotateShow bqVal
              -- annotateShow typ
              -- annotateShow json
              -- annotateShow parsed
              parsed === Right bqVal
    ]
