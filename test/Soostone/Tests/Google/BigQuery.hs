module Soostone.Tests.Google.BigQuery
  ( fastTests,
  )
where

-------------------------------------------------------------------------------

import Control.Lens
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.SafeJSON.Test as SJ
import qualified Data.UUID as UUID
import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Soostone.Google.BigQuery
import Soostone.Tests.Helpers
import Test.Tasty
import Test.Tasty.Hedgehog
import Prelude

-------------------------------------------------------------------------------

fastTests :: TestTree
fastTests =
  testGroup
    "Soostone.Google.BigQuery"
    [ testGroup "CreateTableOptions SafeJSON" $ safeJSONTests genCreateTableOptions,
      testProperty "CreateTableOptions SafeJSON V0 Migration" . property $ do
        optsV0 <- forAll genCreateTableOptions_V0
        liftIO (SJ.migrateRoundTrip @CreateTableOptions optsV0),
      testProperty "calculatePartitionIndex always returns a value from the correct range" . property $ do
        maxPartition <- forAll genPartitionNumber
        uuid <- forAll genUUID
        let inRange pn maxP = pn >= minBound && pn <= maxP
        diff (calculatePartitionIndex maxPartition (UUID.toASCIIBytes uuid)) inRange maxPartition,
      testProperty "Partition bucketing works as expected" . withTests 10 . property $ do
        let sampleSize = 100_000
            partitionSize = (review partitionNumberInt64 maxBound)
        uuids <- Gen.sample (Gen.list (Range.linear sampleSize sampleSize) genUUID)

        let partitions = calculatePartitionIndexDefault . UUID.toASCIIBytes <$> uuids
            mapWithCounts = L.foldl' go M.empty (partitions :: [PartitionNumber])
              where
                go m i = M.insertWith (+) (review partitionNumberInt64 i) (1 :: Int) m
            resultingKeys = M.keys mapWithCounts

        length resultingKeys === fromIntegral partitionSize

        and (fmap (\x -> x > 0 && x <= fromIntegral partitionSize) resultingKeys) === True
    ]
