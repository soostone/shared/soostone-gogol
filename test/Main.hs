module Main
  ( main,
  )
where

-------------------------------------------------------------------------------

import qualified Soostone.Tests.Google
import qualified Soostone.Tests.Google.BigQuery
import qualified Soostone.Tests.Google.BigQuery.Schema
import Test.Tasty
import Prelude

-------------------------------------------------------------------------------

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests =
  testGroup
    "soostone-gogol"
    [ fastTests
    ]

-- | Generally, tests which don't have to really interact with
-- external databases or services.
fastTests :: TestTree
fastTests =
  testGroup
    "fast"
    [ Soostone.Tests.Google.fastTests,
      Soostone.Tests.Google.BigQuery.fastTests,
      Soostone.Tests.Google.BigQuery.Schema.fastTests
    ]
