let sources = import ./nix/sources.nix;
in { ghc ? "ghc964", pkgs ? import sources.nixpkgs {
  config = {
    allowBroken = true;
    allowUnfree = true;
  };
} }:
with pkgs;
let

  inherit (pkgs) callPackage;
  inherit (pkgs.lib) composeExtensions sourceByRegex;
  inherit (pkgs.haskell.lib) dontCheck justStaticExecutables;

  ############################################################################
  # Haskell package set overlay containing overrides for newer packages than
  # are included in the base nixpkgs set as well as additions from vendored
  # or external source repositories.
  haskellPkgSetOverlay =
    callPackage ./nix/haskell/overlay.nix { inherit sources; };

  soostoneGogolOverlay = _self: super: {
    soostone-gogol = super.callCabal2nix "soostone-gogol"
      (lib.sourceByRegex ./. [ "^src.*$" "^test.*$" "package.yaml" ]) { };
  };

  ############################################################################
  # Construct a 'base' Haskell package set
  baseHaskellPkgs = pkgs.haskell.packages.${ghc};

  # Makes overlays given a base haskell package set. Can be used by
  # other projects
  haskellOverlays = [ haskellPkgSetOverlay soostoneGogolOverlay ];

  # Construct the final Haskell package set
  haskellPkgs = baseHaskellPkgs.override (old: {
    overrides = builtins.foldl' composeExtensions (old.overrides or (_: _: { }))
      haskellOverlays;
  });

in {
  inherit haskellPkgs;
  inherit pkgs;
  inherit sources;
  inherit haskellOverlays;
}
