{ haskell, lib, sources }:

# In-order:
#   - the 'final', or fixed-point, Haskell package set environment resulting
#     from the application of this overlay as well as any others it may be
#     composed with
#   - the 'previous' Haskell package set environment that this overlay will have
#     been composed with
#
# Practically speaking:
#   - 'hfinal' can be used to operate over the resulting package set that this
#     overlay may be used to create, however if one is not careful they can
#     cause an infinitely recursive definition by defining something in terms of
#     itself
#   - 'hprev' can be used to operate over the package set environment as it
#     existed strictly _before_ this overlay will have been applied; it can't
#     use the final package set in any way, but it also can't infinitely recurse
#     upon itself
hfinal: hprev:

let inherit (haskell.lib) doJailbreak dontCheck;

in {
  # Gogol got some updates and doesn't currently get hackage releases
  gogol = hfinal.callCabal2nix "gogol" (sources.gogol + "/lib/gogol") { };
  gogol-core =
    hfinal.callCabal2nix "gogol-core" (sources.gogol + "/lib/gogol-core") { };
  gogol-bigquery = hfinal.callCabal2nix "gogol-bigquery"
    (sources.gogol + "/lib/services/gogol-bigquery") { };
  gogol-storage = hfinal.callCabal2nix "gogol-storage"
    (sources.gogol + "/lib/services/gogol-storage") { };
  soostone-error-sum =
    hfinal.callCabal2nix "soostone-error-sum" sources.soostone-error-sum { };
  soostone-tempfile =
    hfinal.callCabal2nix "soostone-tempfile" sources.soostone-tempfile { };

  aeson = hfinal.aeson_2_2_2_0;

  attoparsec-aeson = hfinal.attoparsec-aeson_2_2_2_0;

  fastsum = doJailbreak (hprev.fastsum);

  text-iso8601 = hfinal.text-iso8601_0_1_1;

  # GHC 9.8
  # th-abstraction = hfinal.callHackageDirect {
  #   pkg = "th-abstraction";
  #   ver = "0.6.0.0";
  #   sha256 = "sha256-eOUH1OSmLL8hS8mzijfJvGhxfMEU2vQozKHKlbv2B/A=";
  # } { };

  # th-desugar = hfinal.callHackageDirect {
  #   pkg = "th-desugar";
  #   ver = "1.16";
  #   sha256 = "sha256-qJ5EaThJZZ7RWCudU1Lzjix0T5qgh7bZqz6c8BVwiv0=";
  # } { };

  # singletons-th = hfinal.callHackageDirect {
  #   pkg = "singletons-th";
  #   ver = "3.3";
  #   sha256 = "sha256-JoifQn5f2SOZYw9r/bqtoXVWMALCqMZWbOS0XE05LPk=";
  # } { };
}
